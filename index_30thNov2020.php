<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
require 'Slim/Slim.php';
require 'config.php'; 
require 'polygon.php';
require "sanitise/gump.class.php";
/*============================================================================================*/
$app = new Slim();
$cnf = new Config();
$app->post('/checkuniquecode', 'checkUniqueCode');
$app->post('/userLogin','userLogin');
$app->get('/slc/:ID', 'getSLC');
$app->get('/slc/:ID/:mu', 'getSLC');
$app->get('/slclist/:UserID/:PageNo/:Lat/:Lon', 'getSlcList');
$app->post('/slclist/:UserID/:PageNo/:Lat/:Lon', 'getSlcList');
$app->post('/getSlcListUsingLatLong', 'getSlcListUsingLatLong');
$app->post('/checkmacaddress','checkMacAddress');
$app->post('/checkpoleid','checkPoleID');
$app->post('/checkslcid','checkSlcID');
$app->post('/saveslcdata','saveslcdata');
$app->post('/editslcdata','editslcdata');
$app->post('/addotherdata','addOtherData');
$app->get('/setting/:ID','getSetting');
$app->get('/assets/:ID','getAssets');
$app->get('/assets/:ID/:mu','getAssets');
$app->get('/assets/:ID/:mu/:copy','getAssets');
$app->post('/address','getAddress');
$app->post('/InstalledSlcData','getInstalledSLCData');
$app->get('/sync','syncDataToLGServer');
$app->get('/get-existing-slcs-for-a-client/:clientID','getSLCFromLG');
$app->get('/change-language/:UserID/:Lan/:ClientID/','changeLanguage');
$app->post('/getpoleoptions','getpoleoptions');
$app->post('/checkMacAddressSlcIdbeforeSave','checkMacAddressSlcIdbeforeSave');
$app->post('/checkInternalUniqueMacAddressAPI','checkInternalUniqueMacAddressAPI');
$app->post('/checkInternalUniqueMacAddressEditAPI','checkInternalUniqueMacAddressEditAPI');
$app->post('/checkInternalUniqueSLCIDEditAPI','checkInternalUniqueSLCIDEditAPI');
$app->post('/topbottomlatlng','topbottomlatlng');
$app->run();

function checkUniqueCode()
{
	global $cnf;
	$db 			= 	getConnection();
	$data			= 	Slim::getInstance()->request()->post();
	$code			=	isset($data['code']) && !empty($data['code']) ? trim($data['code']):'';
	$udid	   		= 	isset($data['udid']) && !empty($data['udid'])?$data['udid']:"";
	$language	   	= 	isset($data['language']) && !empty($data['language'])?$data['language']:"en";
	$source 		=	isset($data['source'])?$data['source']:"";
	$ipaddress		= 	$_SERVER['REMOTE_ADDR'];
	$status	   		= 	1;
	$lvars 			= 	getLanguage('None',$language);

	if(!empty($code) && !empty($source) && ($source == 'IOS' || $source == 'Android'))
	{
		$sql 	=	"SELECT a.*,t.label FROM slc_clients AS a LEFT JOIN scl_clients_type AS t ON t.id = a.typeid WHERE role = 'Admin' AND securitycode='".$code."'";
		$stmt	=	$db->query($sql);
		$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ); 
		if (isset($data) && count($data) > 0)
  		{
			if($data[0]->status == 1){
				$sql = "INSERT INTO slc_security_login_attempts(code,source,ipaddress,success)
				VALUES('".$code."','".$source."','".$ipaddress."','Yes')";
				$stmt = $db->query($sql);

				$result =   array();
				$result['ClientID'] =  $data[0]->id;
				if($data[0]->typeid == 4){
					$result['ScanLBL'] =  $lvars['EU_ID'];
					$result['ScanPH'] =  $lvars['EU_ID'];
				}else if($data[0]->typeid == 5){
					$result['ScanLBL'] =  'NID';
					$result['ScanPH'] =  'NID';
				}else if($data[0]->typeid == 7){
					$result['ScanLBL'] =  'IMEI';
					$result['ScanPH'] =  'IMEI';
				}else if($data[0]->typeid == 8){
					$result['ScanLBL'] =  'UID';
					$result['ScanPH'] =  'UID';
				}else{
					$result['ScanLBL'] =  $lvars['MAC_ID'];
					$result['ScanPH'] =  $lvars['MAC_ID'];
				}

				if(isset($data[0]->id)){
					$sql 				= "INSERT INTO slc_users (client_id,device_token,source,status,ipaddress,language) VALUES(".$result['ClientID'].",'".$udid."','".$source."',".$status.",'".$ipaddress."','".$language."')";
					$stmt 				= $db->query($sql);
					$userid 			= $db->lastInsertId();
					$result['userid'] =  $userid;
				}
				$setting	=	getSetting($result['ClientID'],'detail');
				echo json_encode(array("status" => 'success','msg'=>$lvars['VALID_UNIQUE_CODE'],'ClientID'=>$result['ClientID'],'userid'=>$result['userid'],'ScanLBL'=>$result['ScanLBL'],'ScanPH'=>$result['ScanPH'],'data'=>$result,'setting'=>$setting));
			}else{
				$sql = "INSERT INTO slc_security_login_attempts(code,source,ipaddress,success)
				VALUES('".$code."','".$source."','".$ipaddress."','No')";
				$stmt = $db->query($sql);
				echo json_encode(array("status" => 'error','msg'=>$lvars['INACTIVE_ACCOUNT'])); 
			}
		}
		else{
			$sql = "INSERT INTO slc_security_login_attempts(code,source,ipaddress,success)
			VALUES('".$code."','".$source."','".$ipaddress."','No')";
			$stmt = $db->query($sql);
			echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_UNIQUE_CODE'])); 				
		}
	}
	else{
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
	}
    exit();
}
function getUserInfo($id)
{
	$db = getConnection();
	$sql = "SELECT a.* FROM slc_users as a WHERE a.id = ".$id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	if(isset($result[0]))
	{
		return $result[0];
	}
	else
	{
		return array();
	}
}
function getClientInfo($id)
{
	if(isset($id) && !empty($id) && $id > 1)
	{
		$db = getConnection();
		$sql = "SELECT 
						a.*,t.name as typeName,t.label as typeLabel
				FROM 
						slc_clients as a 
						LEFT JOIN scl_clients_type AS t ON t.id = a.typeid
				WHERE 
						status = 1 and a.id = ".$id;
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		if(isset($result[0]))
		{
			return $result[0];
		}
		else
		{
			return array();
		}
	}
	else
	{
		return array();
	}
}
function getLGClientInfo($id)
{
	$db = getConnection();
	$sql = "SELECT a.* FROM slc_clients as a WHERE a.id = ".$id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	if(isset($result[0]))
	{
		return $result[0];
	}
	else
	{
		return array();
	}
}
function userLogin()
{
	global $cnf;
	$db 			= 	getConnection();
	$data			= 	Slim::getInstance()->request()->post();
	$userid			=	isset($data['userid']) && !empty($data['userid']) ? trim($data['userid']):'';
	$client_id		=	isset($data['client_id']) && !empty($data['client_id']) ? trim($data['client_id']):'';
	$username		=	isset($data['username']) && !empty($data['username']) ? trim($data['username']):'';
	$password		=	isset($data['password']) && !empty($data['password']) ? trim($data['password']):'';
	$latitude		=	isset($data['lat'])?$data['lat']:"";
	$longitude		=	isset($data['long'])?$data['long']:"";
	$source 		=	isset($data['source'])?$data['source']:"IOS";
	$Version 		=	isset($data['Version'])?$data['Version']:"5.2.6";
	$ipaddress		= 	$_SERVER['REMOTE_ADDR'];
	$lvars 			= 	getLanguage($userid,'None');
	//check code
	if(!empty($userid) && !empty($client_id) && !empty($username) && !empty($password) && !empty($source) && ($source == 'IOS' || $source == 'Android'))
	//if(!empty($client_id) && !empty($username) && !empty($password) && !empty($source) && ($source == 'IOS' || $source == 'Android'))
	{
		$clientinfo 	= 	getClientInfo($client_id);
		//echo "<pre>";print_r($clientinfo);exit;
		if(isset($clientinfo) && !empty($clientinfo))
		{
			//check login to LG Server			
			if($clientinfo->vLGVersion == 'LG5'){
				//echo $clientinfo->api_server;exit;
				$pos = strpos($clientinfo->api_server, 'api/v5');
				$login_lg_url = substr($clientinfo->api_server,"0", $pos);
				//echo $login_lg_url;exit;
				$LG_LOGIN_URL = $login_lg_url."General/API/";
				//echo $LG_LOGIN_URL;exit;
				$response1	= 	checkLG5Login($username,$password,$clientinfo->clientid,$LG_LOGIN_URL,$lvars,$Version,$source);
				//$response = json_decode($response1, true);
				//echo $response['data']->access_token;exit;
				//echo "<pre>";print_r($response);exit;
				$response 	= json_decode($response1['send_response'], true);

				$token		=	isset($response['data']['access_token'])?$response['data']['access_token']:"";
				$lg_status	=	isset($response["status"])?$response["status"]:"";
				$lg_msg		=	isset($response["message"])?$response["message"]:"";

				if($lg_status == '1')
				{
					//echo $login_lg_url.'/LightingGale/GetClientType';exit;
					$headers 	= array('Content-Type:application/json');
					$ch 		= curl_init();
					curl_setopt($ch, CURLOPT_URL,$login_lg_url.'LightingGale/GetClientType');
					//curl_setopt($ch, CURLOPT_POST, true);
					curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json,Content-Length:','Authorization: bearer '.$token, 'IsSecured:1'));
					curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers)
					{
						$len = strlen($header);
						$header = explode(':', $header, 2);
						if (count($header) >= 2)
							$headers[strtolower(trim($header[0]))] = trim($header[1]);
						return $len;
					});
					$send_response = curl_exec($ch);
					$info = curl_getinfo($ch);
					$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$send_response_from_lg = "";
					//echo "<pre>";print_r($send_response);exit;
					if($httpcode == 200)
					{
						$send_response_from_lg = json_decode($send_response, true);
						//echo "<pre>";print_r($send_response_from_lg);//exit;
						$data1 = $send_response_from_lg['data'];
						$node_types = '';
						for ($i=0; $i < count($data1); $i++) { 
							if(isset($data1[$i]['clientType'])){
								$node_types .= $data1[$i]['clientType']."|";
							}
						}
						//echo substr($node_types, 0, -1);exit;
						$sqll = "UPDATE slc_clients SET node_types = '".substr($node_types, 0, -1)."' WHERE id = ".$client_id;
						$stmtss = $db->query($sqll);
					}

					$sql = "INSERT INTO slc_user_login_attempts(userid,client_id,username,latitude,longitude,source,ipaddress,success,token)
					VALUES(".$userid.",".$client_id.",'".$username."','".$latitude."','".$longitude."','".$source."','".$ipaddress."','Yes','".addslashes($token)."')";
					$stmt = $db->query($sql);

					$sql = "UPDATE slc_users SET username = '".$username."',token = '".addslashes($token)."' WHERE id = ".$userid;
					$stmts = $db->query($sql);
					$db = null;
					$update	=  updateClientAssets($client_id,$clientinfo->api_server,$token,$clientinfo->vLGVersion);
					echo json_encode(array("status" => 'success','msg'=>$lvars['VALID_USER_LOGIN'],'token'=>$token,'node_type_lg'=>$send_response_from_lg));
				}
				else{
					$db = null;
					if($lg_status == '-1'){
						echo json_encode(array("status" => '-1','msg'=>$lg_msg));
					}else{
						echo json_encode(array("status" => '0','msg'=>$lg_msg));
					}
				}
			}else{
				$response	= 	checkLogin($username,$password,$clientinfo->clientid,$clientinfo->api_server,$source,$lvars);
				$token		=	isset($response["Token"])?$response["Token"]:"";
				$return 	= 	str_replace('"',' ',$response["Message"]);
				$return 	= 	trim($return);
				//echo "<pre>";print_r($response);exit;
				if($return == 'Authorized')
				{
					//insert log in db
					$sql = "INSERT INTO slc_user_login_attempts(userid,client_id,username,latitude,longitude,source,ipaddress,success,token)
					VALUES(".$userid.",".$client_id.",'".$username."','".$latitude."','".$longitude."','".$source."','".$ipaddress."','Yes','".addslashes($token)."')";
					$stmt = $db->query($sql);
					//update users name
					$sql = "UPDATE slc_users SET username = '".$username."',token = '".addslashes($token)."' WHERE id = ".$userid;
					$stmts = $db->query($sql);
					//db null
					$db = null;
					//update client assets token
					$update	=  updateClientAssets($client_id,$clientinfo->api_server,$token,$clientinfo->vLGVersion);
					//response
					echo json_encode(array("status" => 'success','msg'=>$lvars['VALID_USER_LOGIN'],'token'=>$token));
				}
				else{
					$db = null;
					echo json_encode(array("status" => 'error','msg'=>$return));
				}
			}
		}
		else
		{
			//db null
			$db = null;
			echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
		}
	}
	else
	{
		//db null
		$db = null;
		//response
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
	}
    exit();
}
function editslcdata()
{
	global $cnf;
	$db 					= 	getConnection();
	$data					= 	Slim::getInstance()->request()->post();
	$ID						=	isset($data['id']) && !empty($data['id']) ? $data['id']:'';
	$slcInfo 		  		= 	getSLCInfo($ID);
	$client_id				=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id				=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$mac_address			=	isset($data['mac_address']) && !empty($data['mac_address']) ? $data['mac_address']:'';
	$slc_id					=	isset($data['slc_id']) && !empty($data['slc_id']) ? $data['slc_id']:'';
	//echo $slc_id;exit;
	$lat					=	isset($data['lat']) && !empty($data['lat']) ? $data['lat']:$slcInfo->lat;
	$lng					=	isset($data['lng']) && !empty($data['lng']) ? $data['lng']:$slcInfo->lng;
	$address				=	isset($data['address']) && !empty($data['address']) ? $data['address']:$slcInfo->address;
	$pole_id				=	isset($data['pole_id']) && !empty($data['pole_id']) ? $data['pole_id']:'None';
	$assets					=	isset($data['Assets']) && !empty($data['Assets']) ? $data['Assets']:'';
	$measurement_units		=	isset($data['measurement_units']) && !empty($data['measurement_units']) ? $data['measurement_units']:$slcInfo->measurement_units;
	$date_of_installation	=	isset($data['date_of_installation']) && !empty($data['date_of_installation']) ? dateConvertToYMD($data['date_of_installation']):$slcInfo->date_of_installation;
	$source 				=	isset($data['source'])?$data['source']:"";
	$notes 					=	isset($data['notes'])?$data['notes']:"";
	$node_type				=	isset($data['node_type']) && !empty($data['node_type']) ? $data['node_type']:'';

	$Camera 					=	isset($data['Camera'])?$data['Camera']:"";
	$Camera_text 				=	isset($data['Camera_text'])?$data['Camera_text']:"";
	$AirQualityMonitor 			=	isset($data['AirQualityMonitor'])?$data['AirQualityMonitor']:"";
	$AirQualityMonitor_text 	=	isset($data['AirQualityMonitor_text'])?$data['AirQualityMonitor_text']:"";
	$Microphone 				=	isset($data['Microphone'])?$data['Microphone']:"";
	$Microphone_text 			=	isset($data['Microphone_text'])?$data['Microphone_text']:"";
	$Radar 						=	isset($data['Radar'])?$data['Radar']:"";
	$Radar_text 				=	isset($data['Radar_text'])?$data['Radar_text']:"";
	$LTRLicensePlateReader 		=	isset($data['LTRLicensePlateReader'])?$data['LTRLicensePlateReader']:"";
	$LTRLicensePlateReader_text =	isset($data['LTRLicensePlateReader_text'])?$data['LTRLicensePlateReader_text']:"";
	$WeatherStation 			=	isset($data['WeatherStation'])?$data['WeatherStation']:"";
	$WeatherStation_text 		=	isset($data['WeatherStation_text'])?$data['WeatherStation_text']:"";
	$PeopleCounter 				=	isset($data['PeopleCounter'])?$data['PeopleCounter']:"";
	$PeopleCounter_text 		=	isset($data['PeopleCounter_text'])?$data['PeopleCounter_text']:"";
	$CarCounter 				=	isset($data['CarCounter'])?$data['CarCounter']:"";
	$CarCounter_text 			=	isset($data['CarCounter_text'])?$data['CarCounter_text']:"";
	$EVCharger 					=	isset($data['EVCharger'])?$data['EVCharger']:"";
	$EVCharger_text 			=	isset($data['EVCharger_text'])?$data['EVCharger_text']:"";
	$RoadTempMonitor 			=	isset($data['RoadTempMonitor'])?$data['RoadTempMonitor']:"";
	$RoadTempMonitor_text 		=	isset($data['RoadTempMonitor_text'])?$data['RoadTempMonitor_text']:"";
	$LEDDisplay 				=	isset($data['LEDDisplay'])?$data['LEDDisplay']:"";
	$LEDDisplay_text 			=	isset($data['LEDDisplay_text'])?$data['LEDDisplay_text']:"";
	$SmallCellularAntenna 		=	isset($data['SmallCellularAntenna'])?$data['SmallCellularAntenna']:"";
	$SmallCellularAntenna_text 	=	isset($data['SmallCellularAntenna_text'])?$data['SmallCellularAntenna_text']:"";
	$FirstResponderAntenna 		=	isset($data['FirstResponderAntenna'])?$data['FirstResponderAntenna']:"";
	$FirstResponderAntenna_text =	isset($data['FirstResponderAntenna_text'])?$data['FirstResponderAntenna_text']:"";
	$PowerMeter 				=	isset($data['PowerMeter'])?$data['PowerMeter']:"";
	$PowerMeter_text 			=	isset($data['PowerMeter_text'])?$data['PowerMeter_text']:"";
	$Other 						=	isset($data['Other'])?$data['Other']:"";
	$Other_text 				=	isset($data['Other_text'])?$data['Other_text']:"";

	$ipaddress				= 	$_SERVER['REMOTE_ADDR'];
	$pole_image				=	"";

	$ClientInfo = getClientInfo($client_id);
	if(isset($ClientInfo) && !empty($ClientInfo))
	{
		if(isset($ClientInfo->vTimeZone) && $ClientInfo->vTimeZone != '') {
			$tz = new DateTimeZone($ClientInfo->vTimeZone);
			$date = new DateTime();
			$date->setTimezone($tz);
			$modified = $date->format('Y-m-d H:i:s');
		}else{
			$modified =	date("Y-m-d H:i:s");
		}
	}else{
		$modified =	date("Y-m-d H:i:s");
		$modified =	date("Y-m-d H:i:s");
	}
	$lvars					=	getLanguage($user_id,'None');
	
	//echo $notes;exit;
	//slc edit
	/*$filename = ROOT_LOGSPATH.'SLC-EDIT-'.$source.'-'.$ipaddress.'-'.time().'.txt';
	$logfile = fopen($filename,"wb");
	fwrite($logfile,json_encode(array('data'=>$data,'ipaddress' =>$_SERVER['REMOTE_ADDR'],'image'=>isset($_FILES)?$_FILES:array())));
	fclose($logfile);*/
	
	//check code
	if(!empty($client_id) && !empty($mac_address) && ($source == 'IOS' || $source == 'Android'))
	{	
		if($slc_id > 0){
			$clientinfo 	= 	getClientInfo($client_id);
			if(isset($clientinfo) && !empty($clientinfo))
			{
				//check mac address validation
				if(isset($data['mac_address']))
				{
					$gump 		= new GUMP();
					$validdata 	= $gump->sanitize($data);
					if($clientinfo->typeid == 5)
					{
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
					}
					elseif($clientinfo->typeid == 4)
					{
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}elseif($clientinfo->typeid == 7)
					{
						$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15|mac_characters'));
					}
					elseif($clientinfo->typeid == 8)
					{
						if($clientinfo->iRPMA == '1' && strlen($mac_address) == 8){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
						}elseif($clientinfo->iOSDI == '1' && strlen($mac_address) == 11){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
						}else if($clientinfo->iNBIOT == '1' && strlen($mac_address) == 15){
							$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
						}else if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && strlen($mac_address) == 16){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
						}else{
							$gump->validation_rules(array('mac_address'=> 'required|invalid'));
						}

						if($clientinfo->iNBIOT == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
							$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
						}
						if($clientinfo->iRPMA == '1' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
						}
						if($clientinfo->iOSDI == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
						}
						if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0'){
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
						}
						/*if($node_type == ''){
							if(strlen($mac_address) == 15){
								$gump->validation_rules(array('mac_address'=> 'required|numeric|min_len,15'));
							}else{
								$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|min_len,15|max_len,16|mac_characters'));
							}
						}
						if($node_type != ''){
							if($node_type == 'NBIOT'){
								$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
							}else{
								$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
							}
						}*/
					}
					else
					{
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}
					$gump->filter_rules(array('mac_address'=> 'trim'));
					$validated_data = $gump->run($validdata);
					if($validated_data === false) 
					{
						$errors = $gump->get_readable_errors(true, '', '', $user_id);
						$errors = str_replace("field",$clientinfo->typeLabel,$errors);
						echo json_encode(array("status" => 'error',"msg" => $errors));
						exit();
					}
					//check unique mac address
					$checkMacAddress = checkUniqueMacAddress($mac_address);
					if($checkMacAddress && $mac_address != $slcInfo->mac_address)
					{
						$errors = $lvars['MAC_ADDRSS_ALREADY_USED'];
						$errors = str_replace("Value",$clientinfo->typeLabel,$errors);
						echo json_encode(array("status" => 'error','msg'=>$errors));
						exit();
					}
				}
				
				if(isset($data['slc_id']))
				{
					$gump 		= new GUMP();
					$validdata 	= $gump->sanitize($data);
					if($clientinfo->typeid == 1 || $clientinfo->typeid == 4)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,65535'));
					}
					elseif($clientinfo->typeid == 2 || $clientinfo->typeid == 5 || $clientinfo->typeid == 6)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,99999999'));
					}
					elseif($clientinfo->typeid == 8)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,4294967295'));
					}
					else
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,0000000099999999'));
					}
					$gump->filter_rules(array('slc_id'=> 'trim'));
					$validated_data = $gump->run($validdata);
					if($validated_data === false) 
					{
						$errors = $gump->get_readable_errors(true, '', '', $user_id);
						$errors = str_replace("field","SLC ID",$errors);
						echo json_encode(array("status" => 'error',"msg" => $errors));
						exit();
					}
					//check unique SLC ID
					$checkSlc_id	  =	 checkUniqueSLCID($client_id,$slc_id);
					if($checkSlc_id && $slc_id != $slcInfo->slc_id)
					{
						echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID_ALREADY_USED']));
						exit();
					}
					elseif(!is_numeric($slc_id))
					{
						echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID_NOT_NUMERIC']));
						exit();
					}
				}
				//check Pole ID validation
				/*if(isset($data['pole_id']) && !empty($data['pole_id']) &&  $data['pole_id'] != "None")
				{
					$checkPoleID = checkPoleIDUniqueness($pole_id,$client_id);
					if(isset($checkPoleID) && $checkPoleID > 0 && $pole_id != $slcInfo->pole_id)
					{
						echo json_encode(array("status" => 'error','msg'=> $lvars['POLE_ID_ALREADY_USED']));
						exit();
					}
				}*/
				//get assets
				if(isset($assets))
				{
					$assets = getConvertAssets($assets);
				}
				else
				{
					$assets = "";
				}
				//upload pole images
				if(isset($_FILES['pole_image']['name']) && $_FILES['pole_image']['name']!="")
				{
					$pole_image	=	uploadPoleImg($slc_id,$_FILES);
				}
				else
				{
					if(isset($slcInfo->pole_image) && !empty($slcInfo->pole_image))
					{
						$pole_image	= $slcInfo->pole_image;
					}
				}
				$data['pole_image'] = $pole_image;

				$checkInternalMacAddress = checkInternalUniqueMacAddress($mac_address);
				if($checkInternalMacAddress){
				//update query			
					$sql = "UPDATE 
									slc_installation_data 
							SET
									mac_address = '',
									slc_id = '',
									internal_mac_address = '".addslashes($mac_address)."',
									internal_slc_id = '".addslashes($slc_id)."',
									lat = '".addslashes($lat)."',
									lng = '".addslashes($lng)."',
									address = '".addslashes($address)."',
									measurement_units = '".addslashes($measurement_units)."',
									pole_id = '".addslashes($pole_id)."',
									pole_image = '".addslashes($pole_image)."',
									assets = '".addslashes($assets)."',
									date_of_installation = '".addslashes($date_of_installation)."',
									notes = '".addslashes($notes)."',
									modified = '".addslashes($modified)."',
									Camera = '".$Camera."',
									Camera_text = '".addslashes($Camera_text)."',
									AirQualityMonitor = '".$AirQualityMonitor."',
									AirQualityMonitor_text = '".addslashes($AirQualityMonitor_text)."',
									Microphone = '".$Microphone."',
									Microphone_text = '".addslashes($Microphone_text)."',
									Radar = '".$Radar."',
									Radar_text = '".addslashes($Radar_text)."',
									LTRLicensePlateReader = '".$LTRLicensePlateReader."',
									LTRLicensePlateReader_text = '".addslashes($LTRLicensePlateReader_text)."',
									WeatherStation = '".$WeatherStation."',
									WeatherStation_text = '".addslashes($WeatherStation_text)."',
									PeopleCounter = '".$PeopleCounter."',
									PeopleCounter_text = '".addslashes($PeopleCounter_text)."',
									CarCounter = '".$CarCounter."',
									CarCounter_text = '".addslashes($CarCounter_text)."',
									EVCharger = '".$EVCharger."',
									EVCharger_text = '".addslashes($EVCharger_text)."',
									RoadTempMonitor = '".$RoadTempMonitor."',
									RoadTempMonitor_text = '".addslashes($RoadTempMonitor_text)."',
									LEDDisplay = '".$LEDDisplay."',
									LEDDisplay_text = '".addslashes($LEDDisplay_text)."',
									SmallCellularAntenna = '".$SmallCellularAntenna."',
									SmallCellularAntenna_text = '".addslashes($SmallCellularAntenna_text)."',
									FirstResponderAntenna = '".$FirstResponderAntenna."',
									FirstResponderAntenna_text = '".addslashes($FirstResponderAntenna_text)."',
									PowerMeter = '".$PowerMeter."',
									PowerMeter_text = '".addslashes($PowerMeter_text)."',
									Other = '".$Other."',
									node_type = '".$node_type."',
									Other_text = '".addslashes($Other_text)."'
							WHERE 
									ID = ".$ID;
					$stmts = $db->query($sql);
				}else{
					$sql = "UPDATE 
									slc_installation_data 
							SET
									mac_address = '".addslashes($mac_address)."',
									slc_id = '".addslashes($slc_id)."',
									internal_mac_address = '',
									internal_slc_id = '',
									lat = '".addslashes($lat)."',
									lng = '".addslashes($lng)."',
									address = '".addslashes($address)."',
									measurement_units = '".addslashes($measurement_units)."',
									pole_id = '".addslashes($pole_id)."',
									pole_image = '".addslashes($pole_image)."',
									assets = '".addslashes($assets)."',
									date_of_installation = '".addslashes($date_of_installation)."',
									notes = '".addslashes($notes)."',
									modified = '".addslashes($modified)."',
									Camera = '".$Camera."',
									Camera_text = '".addslashes($Camera_text)."',
									AirQualityMonitor = '".$AirQualityMonitor."',
									AirQualityMonitor_text = '".addslashes($AirQualityMonitor_text)."',
									Microphone = '".$Microphone."',
									Microphone_text = '".addslashes($Microphone_text)."',
									Radar = '".$Radar."',
									Radar_text = '".addslashes($Radar_text)."',
									LTRLicensePlateReader = '".$LTRLicensePlateReader."',
									LTRLicensePlateReader_text = '".addslashes($LTRLicensePlateReader_text)."',
									WeatherStation = '".$WeatherStation."',
									WeatherStation_text = '".addslashes($WeatherStation_text)."',
									PeopleCounter = '".$PeopleCounter."',
									PeopleCounter_text = '".addslashes($PeopleCounter_text)."',
									CarCounter = '".$CarCounter."',
									CarCounter_text = '".addslashes($CarCounter_text)."',
									EVCharger = '".$EVCharger."',
									EVCharger_text = '".addslashes($EVCharger_text)."',
									RoadTempMonitor = '".$RoadTempMonitor."',
									RoadTempMonitor_text = '".addslashes($RoadTempMonitor_text)."',
									LEDDisplay = '".$LEDDisplay."',
									LEDDisplay_text = '".addslashes($LEDDisplay_text)."',
									SmallCellularAntenna = '".$SmallCellularAntenna."',
									SmallCellularAntenna_text = '".addslashes($SmallCellularAntenna_text)."',
									FirstResponderAntenna = '".$FirstResponderAntenna."',
									FirstResponderAntenna_text = '".addslashes($FirstResponderAntenna_text)."',
									PowerMeter = '".$PowerMeter."',
									PowerMeter_text = '".addslashes($PowerMeter_text)."',
									Other = '".$Other."',
									node_type = '".$node_type."',
									Other_text = '".addslashes($Other_text)."'
							WHERE 
									ID = ".$ID;
					$stmts = $db->query($sql);
				}
				AutoSyncSLCStatus($ID);
				//creted logs
				$logs = slcLog($ID,$data);
				$db = null;
				//slc edit
				/*$filename = ROOT_LOGSPATH.'SLC-EDIT-SUCCESS-'.$source.'-'.$ipaddress.'-'.time().'.txt';
				$logfile = fopen($filename,"wb");
				fwrite($logfile,json_encode(array('data'=>$data)));
				fclose($logfile);*/
				echo json_encode(array("status" => 'success','msg'=>$lvars['SLC_DATA_EDITED']));	
			}
			else
			{
				echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
			}
		}else{
			echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID']." ".$lvars['RULE_26']));
		}
	}
	else
	{	
		//response
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
	}
    exit();
}
function checkMacAddress()
{
	global $cnf;
	$data		= 	Slim::getInstance()->request()->post();
	$client_id	=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id	=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$node_type	=	isset($data['node_type']) && !empty($data['node_type']) ? $data['node_type']:'';
	$macaddress	=	isset($data['macaddress']) && !empty($data['macaddress']) ? $data['macaddress']:'';
	$lvars			=	getLanguage($user_id,'None');
	$clientinfo 	= 	getClientInfo($client_id);
	if(isset($clientinfo) && !empty($clientinfo) && isset($user_id) && !empty($user_id))
	{
		$gump 		= new GUMP();
		$validdata 	= $gump->sanitize($data);
		if($clientinfo->typeid == 5)
		{
			$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
		}
		elseif($clientinfo->typeid == 4)
		{
			$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
		}elseif($clientinfo->typeid == 7)
		{
			$gump->validation_rules(array('macaddress'=> 'required|numeric|len_equal,15|mac_characters'));
		}
		elseif($clientinfo->typeid == 8)
		{
			//if($clientinfo->typename != ''){
				//$typenames = explode("|", $clientinfo->typename);
				/*if(count($typenames) > 1){
					echo "<pre>";print_r($typenames);
					for ($i=0; $i < count($typenames); $i++) { 
						if(($typenames[$i] == 'Zigbee' || $typenames[$i] == 'Cisco') && strlen($macaddress) == 16){
							$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
						}
						else if($typenames[$i] == 'NBIoT' && strlen($macaddress) == 15){
							$gump->validation_rules(array('macaddress'=> 'required|numeric|len_equal,15'));
						}
						else if($typenames[$i] == 'OSDI' && strlen($macaddress) == 11){
							$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
						}
						else if($typenames[$i] == 'RPMA' && strlen($macaddress) == 8){
							$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
						}
						else{
							//if((strlen($macaddress) == 8 && $typenames[$i] == 'RPMA') || (strlen($macaddress) == 11 && $typenames[$i] == 'OSDI') || (strlen($macaddress) == 15 && $typenames[$i] == 'NBIoT') || (strlen($macaddress) == 16 && $typenames[$i] == 'Zigbee')) {
								$gump->validation_rules(array('macaddress'=> 'required|mac_characters'));
							//}else{
								//$gump->validation_rules(array('macaddress'=> 'required|invalid|mac_characters'));
							//}
						}
					}
				}else{*/
					if($clientinfo->iRPMA == '1' && strlen($macaddress) == 8){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
					}elseif($clientinfo->iOSDI == '1' && strlen($macaddress) == 11){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
					}else if($clientinfo->iNBIOT == '1' && strlen($macaddress) == 15){
						$gump->validation_rules(array('macaddress'=> 'required|numeric|len_equal,15'));
					}else if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && strlen($macaddress) == 16){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}else{
						$gump->validation_rules(array('macaddress'=> 'required|invalid'));
					}

					if($clientinfo->iNBIOT == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('macaddress'=> 'required|numeric|len_equal,15'));
					}
					if($clientinfo->iRPMA == '1' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
					}
					if($clientinfo->iOSDI == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
					}
					if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0'){
						$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}

				//}
			//}
			/*if($node_type == ''){
				if(strlen($macaddress) == 15){
					$gump->validation_rules(array('macaddress'=> 'required|numeric|min_len,15'));
				}else{
					$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|min_len,15|max_len,16|mac_characters'));
				}
			}
			if($node_type != ''){
				if($node_type == 'NBIOT'){
					$gump->validation_rules(array('macaddress'=> 'required|numeric|len_equal,15'));
				}else{
					$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
				}
			}*/
		}
		else
		{
			$gump->validation_rules(array('macaddress'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
		}
		$gump->filter_rules(array('macaddress'=> 'trim'));
		$validated_data = $gump->run($validdata);

		if($clientinfo->typeLabel == 'MAC ID'){
			$TypeLabel = $lvars['MAC_ID'];
		}else if($clientinfo->typeLabel == 'IMEI'){
			$TypeLabel = "IMEI";
		}else if($clientinfo->typeid == '8'){
			$TypeLabel = "UID";
		}else{
			$TypeLabel = $lvars['EU_ID'];
		}
		//echo "<pre>";print_r($validated_data);
		if($validated_data === false) 
		{	
			$errors = $gump->get_readable_errors(true, '', '', $user_id);
			$errors = str_replace("field",$TypeLabel,$errors);
			echo json_encode(array("status" => 'error',"msg" => $errors));
		}
		else
		{
			/*$checkMacAddress = checkUniqueMacAddressUniqueness($macaddress,$client_id,$user_id);
			if($checkMacAddress['status'] == "error")
			{
				$errors = $lvars['MAC_ADDRSS_ALREADY_USED'];
				$errors = str_replace("Value",$TypeLabel,$errors);
				echo json_encode(array("status" => 'error','msg'=>$errors));
			}
			else
			{*/
				$errors = $lvars['MAC_ADDRSS_VALID_USED'];
				$errors = str_replace("Value",$TypeLabel,$errors);
				echo json_encode(array("status" => 'success','msg'=>$lvars['MAC_ADDRSS_VALID_USED']));
			//}
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}	
	exit();
}
function checkSlcID()
{
	global $cnf;
	$data			= 	Slim::getInstance()->request()->post();
	$client_id		=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id		=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$slcid			=	isset($data['slcid']) && !empty($data['slcid']) ? $data['slcid']:'';
	$lvars			=	getLanguage($user_id,'None');
	$clientinfo 	= 	getClientInfo($client_id);
	if(isset($clientinfo) && !empty($clientinfo))
	{
		if($slcid > 0){
			$gump 		= new GUMP();
			$validdata 	= $gump->sanitize($data);
			if($clientinfo->typeid == 1 || $clientinfo->typeid == 4)
			{
				$gump->validation_rules(array('slcid'=> 'required|numeric|min_len,1|max_numeric,65535'));
			}
			elseif($clientinfo->typeid == 2 || $clientinfo->typeid == 5 || $clientinfo->typeid == 6)
			{
				$gump->validation_rules(array('slcid'=> 'required|numeric|min_len,1|max_numeric,99999999'));
			}
			elseif($clientinfo->typeid == 8)
			{
				$gump->validation_rules(array('slcid'=> 'required|numeric|min_len,1|max_numeric,4294967295'));
			}
			else
			{
				$gump->validation_rules(array('slcid'=> 'required|numeric|min_len,1|max_numeric,0000000099999999'));
			}

			//$gump->validation_rules(array('slcid'=> 'required|integer|min_len,1|max_len,16'));
			
			/* Allow Alpha numeric characters as suggested by Marsel 5/25/2019- VIRAJ
			$gump->validation_rules(array('slcid'=> 'required|alpha_numeric|min_len,1|max_len,16'));*/

			/* Allow Only numeric characters as suggested by Viraj 07/05/2019 - Rahul*/
			//$gump->validation_rules(array('slcid'=> 'required|numeric|min_len,1|max_len,16'));

			$gump->filter_rules(array('slcid'=> 'trim'));
			$validated_data = $gump->run($validdata);
			if($validated_data === false) 
			{
				$errors = $gump->get_readable_errors(true, '', '', $user_id);
				$errors = str_replace("field",$lvars['SLC_ID'],$errors);
				echo json_encode(array("status" => 'error',"msg" => $errors));
			}
			else
			{
				echo json_encode(array("status" => 'success','msg'=>$lvars['SLC_ID_VALID_USED']));
			}
		}else{
			echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID']." ".$lvars['RULE_26']));
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}	
	exit();
}
function saveslcdata()
{
	global $cnf;
	$db 					= 	getConnection();
	$data					= 	Slim::getInstance()->request()->post();
	$client_id				=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id				=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$mac_address			=	isset($data['mac_address']) && !empty($data['mac_address']) ? $data['mac_address']:'';
	$slc_id					=	isset($data['slc_id']) && !empty($data['slc_id']) ? $data['slc_id']:'';
	$lat					=	isset($data['lat']) && !empty($data['lat']) ? $data['lat']:'';
	$lng					=	isset($data['lng']) && !empty($data['lng']) ? $data['lng']:'';
	$address				=	isset($data['address']) && !empty($data['address']) ? $data['address']:'';
	$measurement_units		=	isset($data['measurement_units']) && !empty($data['measurement_units']) ? $data['measurement_units']:'None';
	$pole_id				=	isset($data['pole_id']) && !empty($data['pole_id']) ? $data['pole_id']:'None';
	$skip					=	isset($data['skip']) && !empty($data['skip']) ? $data['skip']:'No';
	$assets					=	isset($data['Assets']) && !empty($data['Assets']) ? $data['Assets']:'';
	$date_of_installation	=	isset($data['date_of_installation']) && !empty($data['date_of_installation']) ? dateConvertToYMD($data['date_of_installation']):date('Y-m-d');
	$source 				=	isset($data['source'])?$data['source']:"IOS";
	$copyasset 				=	isset($data['copyasset']) && !empty($data['copyasset']) && trim($data['copyasset']) == "Yes" ? "Yes" : "No";
	$SLCID					=	isset($data['SLCID']) && !empty($data['SLCID']) ? $data['SLCID']:0;
	$ipaddress				= 	$_SERVER['REMOTE_ADDR'];
	$pole_image				=	"";
	$notes					=	isset($data['notes']) && !empty($data['notes']) ? $data['notes']:'';
	$node_type				=	isset($data['node_type']) && !empty($data['node_type']) ? $data['node_type']:'';

	$Camera 					=	isset($data['Camera'])?$data['Camera']:"";
	//echo $Camera;exit;
	$Camera_text 				=	isset($data['Camera_text'])?$data['Camera_text']:"";
	$AirQualityMonitor 			=	isset($data['AirQualityMonitor'])?$data['AirQualityMonitor']:"";
	$AirQualityMonitor_text 	=	isset($data['AirQualityMonitor_text'])?$data['AirQualityMonitor_text']:"";
	$Microphone 				=	isset($data['Microphone'])?$data['Microphone']:"";
	$Microphone_text 			=	isset($data['Microphone_text'])?$data['Microphone_text']:"";
	$Radar 						=	isset($data['Radar'])?$data['Radar']:"";
	$Radar_text 				=	isset($data['Radar_text'])?$data['Radar_text']:"";
	$LTRLicensePlateReader 		=	isset($data['LTRLicensePlateReader'])?$data['LTRLicensePlateReader']:"";
	$LTRLicensePlateReader_text =	isset($data['LTRLicensePlateReader_text'])?$data['LTRLicensePlateReader_text']:"";
	$WeatherStation 			=	isset($data['WeatherStation'])?$data['WeatherStation']:"";
	$WeatherStation_text 		=	isset($data['WeatherStation_text'])?$data['WeatherStation_text']:"";
	$PeopleCounter 				=	isset($data['PeopleCounter'])?$data['PeopleCounter']:"";
	$PeopleCounter_text 		=	isset($data['PeopleCounter_text'])?$data['PeopleCounter_text']:"";
	$CarCounter 				=	isset($data['CarCounter'])?$data['CarCounter']:"";
	$CarCounter_text 			=	isset($data['CarCounter_text'])?$data['CarCounter_text']:"";
	$EVCharger 					=	isset($data['EVCharger'])?$data['EVCharger']:"";
	$EVCharger_text 			=	isset($data['EVCharger_text'])?$data['EVCharger_text']:"";
	$RoadTempMonitor 			=	isset($data['RoadTempMonitor'])?$data['RoadTempMonitor']:"";
	$RoadTempMonitor_text 		=	isset($data['RoadTempMonitor_text'])?$data['RoadTempMonitor_text']:"";
	$LEDDisplay 				=	isset($data['LEDDisplay'])?$data['LEDDisplay']:"";
	$LEDDisplay_text 			=	isset($data['LEDDisplay_text'])?$data['LEDDisplay_text']:"";
	$SmallCellularAntenna 		=	isset($data['SmallCellularAntenna'])?$data['SmallCellularAntenna']:"";
	$SmallCellularAntenna_text 	=	isset($data['SmallCellularAntenna_text'])?$data['SmallCellularAntenna_text']:"";
	$FirstResponderAntenna 		=	isset($data['FirstResponderAntenna'])?$data['FirstResponderAntenna']:"";
	$FirstResponderAntenna_text =	isset($data['FirstResponderAntenna_text'])?$data['FirstResponderAntenna_text']:"";
	$PowerMeter 				=	isset($data['PowerMeter'])?$data['PowerMeter']:"";
	$PowerMeter_text 			=	isset($data['PowerMeter_text'])?$data['PowerMeter_text']:"";
	$Other 						=	isset($data['Other'])?$data['Other']:"";
	$Other_text 				=	isset($data['Other_text'])?$data['Other_text']:"";

	$ClientInfo = getClientInfo($client_id);
	if(isset($ClientInfo) && !empty($ClientInfo))
	{
		if(isset($ClientInfo->vTimeZone) && $ClientInfo->vTimeZone != '') {
			$tz = new DateTimeZone($ClientInfo->vTimeZone);
			$date = new DateTime();
			$date->setTimezone($tz);
			$modified = $date->format('Y-m-d H:i:s');
			$created = $date->format('Y-m-d H:i:s');
		}else{
			$modified =	date("Y-m-d H:i:s");
			$created =	date("Y-m-d H:i:s");
		}
	}else{
		$modified =	date("Y-m-d H:i:s");
		$modified =	date("Y-m-d H:i:s");
	}

	$lvars					=	getLanguage($user_id,'None');
	if(!empty($user_id) && !empty($client_id) && !empty($mac_address) && !empty($lat) && !empty($lng) && ($source == 'IOS' || $source == 'Android'))
	{
		if($slc_id > 0){
			$clientinfo 	= 	getClientInfo($client_id);
			if(isset($clientinfo) && !empty($clientinfo))
			{
				$gump 		= new GUMP();
				$validdata 	= $gump->sanitize($data);
				if($clientinfo->typeid == 5)
				{
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
				}
				elseif($clientinfo->typeid == 4)
				{
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
				}elseif($clientinfo->typeid == 7)
				{
					$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15|mac_characters'));
				}
				elseif($clientinfo->typeid == 8)
				{
					if($clientinfo->iRPMA == '1' && strlen($mac_address) == 8){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
					}elseif($clientinfo->iOSDI == '1' && strlen($mac_address) == 11){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
					}else if($clientinfo->iNBIOT == '1' && strlen($mac_address) == 15){
						$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
					}else if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && strlen($mac_address) == 16){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}else{
						$gump->validation_rules(array('mac_address'=> 'required|invalid'));
					}

					if($clientinfo->iNBIOT == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
					}
					if($clientinfo->iRPMA == '1' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
					}
					if($clientinfo->iOSDI == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
					}
					if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0'){
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}
					/*if($node_type == ''){
						if(strlen($mac_address) == 15){
							$gump->validation_rules(array('mac_address'=> 'required|numeric|min_len,15'));
						}else{
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|min_len,15|max_len,16|mac_characters'));
						}
					}
					if($node_type != ''){
						if($node_type == 'NBIOT'){
							$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
						}else{
							$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
						}
					}*/
				}
				else
				{
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
				}
				$gump->filter_rules(array('mac_address'=> 'trim'));
				$validated_data = $gump->run($validdata);
				if($validated_data === false){
					$errors = $gump->get_readable_errors(true, '', '', $user_id);
					$errors = str_replace("field",$clientinfo->typeLabel,$errors);
					echo json_encode(array("status" => 'error',"msg" => $errors));
					exit();
				}

				if(isset($data['slc_id']))
				{
					$gump 		= new GUMP();
					$validdata 	= $gump->sanitize($data);
					
					if($clientinfo->typeid == 1 || $clientinfo->typeid == 4)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,65535'));
					}
					elseif($clientinfo->typeid == 2 || $clientinfo->typeid == 5 || $clientinfo->typeid == 6)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,99999999'));
					}
					elseif($clientinfo->typeid == 8)
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,4294967295'));
					}
					else
					{
						$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,0000000099999999'));
					}

					$gump->filter_rules(array('slc_id'=> 'trim'));
					$validated_data = $gump->run($validdata);
					if($validated_data === false) 
					{
						$errors = $gump->get_readable_errors(true, '', '', $user_id);
						$errors = str_replace("field","SLC ID",$errors);
						echo json_encode(array("status" => 'error',"msg" => $errors));
						exit();
					}
					if(!is_numeric($slc_id))
					{
						echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID_NOT_NUMERIC']));
						exit();
					}
				}
				
				$checkInternalMacAddress = checkInternalUniqueMacAddress($mac_address);
				if($checkInternalMacAddress){
					if(isset($_FILES['pole_image']['name']) && $_FILES['pole_image']['name']!=""){
						$pole_image		=	uploadPoleImg($slc_id,$_FILES);
					}

					$sqll 	=	"SELECT ID FROM slc_installation_data WHERE internal_mac_address = '".$mac_address."' AND client_id = '".$client_id."' LIMIT 1";
					$stmt	=	$db->query($sqll);
					$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);

					$del_sql = "DELETE FROM slc_installation_data WHERE ID = ".$data[0]->ID." LIMIT 1";
					$del_result = $db->query($del_sql);
					if(isset($assets)){
						$assets = getConvertAssets($assets);
					}

					//$assets_key = '';
					//$assets_value = '';
					//if($skip == 'No'){
						$assets_key = ', assets';
						$assets_value = ", '".addslashes($assets)."'";
					/*}else{
						$assets_key = '';
						$assets_value = '';
					}*/

					$sql = "INSERT INTO slc_installation_data (client_id,
											  user_id,
											  internal_mac_address,
											  internal_slc_id,
											  lat,
											  lng,
											  address,
											  measurement_units,
											  pole_id,
											  pole_image,
											  date_of_installation,
											  created,
											  created_by,
											  ipaddress,
											  source,
											  notes,
											  Camera,
											  Camera_text,
											  AirQualityMonitor,
											  AirQualityMonitor_text,
											  Microphone,
											  Microphone_text,
											  Radar,
											  Radar_text,
											  LTRLicensePlateReader,
											  LTRLicensePlateReader_text,
											  WeatherStation,
											  WeatherStation_text,
											  PeopleCounter,
											  PeopleCounter_text,
											  CarCounter,
											  CarCounter_text,
											  EVCharger,
											  EVCharger_text,
											  RoadTempMonitor,
											  RoadTempMonitor_text,
											  LEDDisplay,
											  LEDDisplay_text,
											  SmallCellularAntenna,
											  SmallCellularAntenna_text,
											  FirstResponderAntenna,
											  FirstResponderAntenna_text,
											  PowerMeter,
											  PowerMeter_text,
											  Other,
											  Other_text,node_type".$assets_key.")
											  VALUES
											  (".$client_id.",
											  ".$user_id.",
											  '".addslashes($mac_address)."',
											  '".addslashes($slc_id)."',
											  '".addslashes($lat)."',
											  '".addslashes($lng)."',
											  '".addslashes($address)."',
											  '".addslashes($measurement_units)."',
											  '".addslashes($pole_id)."',
											  '".addslashes($pole_image)."',
											  '".addslashes($date_of_installation)."',
											  '".addslashes($created)."',
											  ".$user_id.",
											  '".$ipaddress."',
											  '".$source."', 
											  '".addslashes($notes)."',
											  '".$Camera."',
											  '".addslashes($Camera_text)."',
											  '".$AirQualityMonitor."',
											  '".addslashes($AirQualityMonitor_text)."',
											  '".$Microphone."',
											  '".addslashes($Microphone_text)."',
											  '".$Radar."',
											  '".addslashes($Radar_text)."',
											  '".$LTRLicensePlateReader."',
											  '".addslashes($LTRLicensePlateReader_text)."',
											  '".$WeatherStation."',
											  '".addslashes($WeatherStation_text)."',
											  '".$PeopleCounter."',
											  '".addslashes($PeopleCounter_text)."',
											  '".$CarCounter."',
											  '".addslashes($CarCounter_text)."',
											  '".$EVCharger."',
											  '".addslashes($EVCharger_text)."',
											  '".$RoadTempMonitor."',
											  '".addslashes($RoadTempMonitor_text)."',
											  '".$LEDDisplay."',
											  '".addslashes($LEDDisplay_text)."',
											  '".$SmallCellularAntenna."',
											  '".addslashes($SmallCellularAntenna_text)."',
											  '".$FirstResponderAntenna."',
											  '".addslashes($FirstResponderAntenna_text)."',
											  '".$PowerMeter."',
											  '".addslashes($PowerMeter_text)."',
											  '".$Other."',
											  '".addslashes($Other_text)."',
											  '".$node_type."' ".$assets_value.")";
					$stmt 				= $db->query($sql);
					$lastInsertId 		= $db->lastInsertId();
					if(isset($lastInsertId)){
						$synch = AutoSyncSLCStatus($lastInsertId);
					}

					if($skip == 'No'){				
						$success = $lvars['SLC_DATA_SAVED'];
					}
					else{
						$success = $lvars['SLC_DATA_SKIP'];
					}

					$errors = $lvars['MAC_ADDRSS_ALREADY_USED'];
					$errors = str_replace("Value",$clientinfo->typeLabel,$errors);
					echo json_encode(array("status" => 'success','msg'=>$success));
				}else{
					$checkMacAddress = checkUniqueMacAddress($mac_address);
					$checkSlc_id	 = checkUniqueSLCID($client_id,$slc_id);
					if($checkMacAddress)
					{
						if(isset($assets)){
							$assets = getConvertAssets($assets);
						}

						//$assets_value = '';
						//if($skip == 'No'){
							$assets_value = ", assets = '".addslashes($assets)."'";
						/*}else{
							$assets_value = '';
						}*/

						$sql = "UPDATE slc_installation_data SET client_id = '".$client_id."',
						  	user_id = '".$user_id."',
						  	lat = '".addslashes($lat)."',
						  	lng = '".addslashes($lng)."',
						  	address = '".addslashes($address)."',
						  	measurement_units = '".addslashes($measurement_units)."',
						  	pole_id = '".addslashes($pole_id)."',
						  	pole_image = '".addslashes($pole_image)."',
						  	date_of_installation = '".addslashes($date_of_installation)."',
						  	modified = '".addslashes($modified)."',
						  	created_by = '".$user_id."',
						  	ipaddress = '".$ipaddress."',
						  	notes = '".addslashes($notes)."',
						  	source = '".$source."',
						  	Camera = '".$Camera."',
							Camera_text = '".addslashes($Camera_text)."',
							AirQualityMonitor = '".$AirQualityMonitor."',
							AirQualityMonitor_text = '".addslashes($AirQualityMonitor_text)."',
							Microphone = '".$Microphone."',
							Microphone_text = '".addslashes($Microphone_text)."',
							Radar = '".$Radar."',
							Radar_text = '".addslashes($Radar_text)."',
							LTRLicensePlateReader = '".$LTRLicensePlateReader."',
							LTRLicensePlateReader_text = '".addslashes($LTRLicensePlateReader_text)."',
							WeatherStation = '".$WeatherStation."',
							WeatherStation_text = '".addslashes($WeatherStation_text)."',
							PeopleCounter = '".$PeopleCounter."',
							PeopleCounter_text = '".addslashes($PeopleCounter_text)."',
							CarCounter = '".$CarCounter."',
							CarCounter_text = '".addslashes($CarCounter_text)."',
							EVCharger = '".$EVCharger."',
							EVCharger_text = '".addslashes($EVCharger_text)."',
							RoadTempMonitor = '".$RoadTempMonitor."',
							RoadTempMonitor_text = '".addslashes($RoadTempMonitor_text)."',
							LEDDisplay = '".$LEDDisplay."',
							LEDDisplay_text = '".addslashes($LEDDisplay_text)."',
							SmallCellularAntenna = '".$SmallCellularAntenna."',
							SmallCellularAntenna_text = '".addslashes($SmallCellularAntenna_text)."',
							FirstResponderAntenna = '".$FirstResponderAntenna."',
							FirstResponderAntenna_text = '".addslashes($FirstResponderAntenna_text)."',
							PowerMeter = '".$PowerMeter."',
							PowerMeter_text = '".addslashes($PowerMeter_text)."',
							Other = '".$Other."',
							node_type = '".addslashes($node_type)."',
							Other_text = '".addslashes($Other_text)."'".$assets_value." 
							WHERE mac_address = '".$mac_address."'";
						$update = $db->query($sql);

						$sqll 	=	"SELECT ID FROM slc_installation_data WHERE mac_address = '".$mac_address."' LIMIT 1";
						$stmt	=	$db->query($sqll);
						$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
						if(isset($data[0]->ID)){
							$synch = AutoSyncSLCStatus($data[0]->ID);
						}

						$msg 	= "Your data is saved. You will be able to add/edit this data later from List -> Edit";
						$success = '';
						$success .= "$msg";

						$errors = $lvars['MAC_ADDRSS_ALREADY_USED'];
						$errors = str_replace("Value",$clientinfo->typeLabel,$errors);
						echo json_encode(array("status" => 'success','msg'=>$success));
					}
					elseif($checkSlc_id){
						if(isset($assets)){
							$assets = getConvertAssets($assets);
						}

						//$assets_value = '';
						//if($skip == 'No'){
							$assets_value = ", assets = '".addslashes($assets)."'";
						/*}else{
							$assets_value = '';
						}*/

						$sql = "UPDATE slc_installation_data SET client_id = '".$client_id."',
						  	user_id = '".$user_id."',
						  	lat = '".addslashes($lat)."',
						  	lng = '".addslashes($lng)."',
						  	address = '".addslashes($address)."',
						  	measurement_units = '".addslashes($measurement_units)."',
						  	pole_id = '".addslashes($pole_id)."',
						  	pole_image = '".addslashes($pole_image)."',
						  	date_of_installation = '".addslashes($date_of_installation)."',
						  	modified = '".addslashes($modified)."',
						  	created_by = '".$user_id."',
						  	ipaddress = '".$ipaddress."',
						  	notes = '".addslashes($notes)."',
						  	source = '".$source."',
						  	Camera = '".$Camera."',
							Camera_text = '".addslashes($Camera_text)."',
							AirQualityMonitor = '".$AirQualityMonitor."',
							AirQualityMonitor_text = '".addslashes($AirQualityMonitor_text)."',
							Microphone = '".$Microphone."',
							Microphone_text = '".addslashes($Microphone_text)."',
							Radar = '".$Radar."',
							Radar_text = '".addslashes($Radar_text)."',
							LTRLicensePlateReader = '".$LTRLicensePlateReader."',
							LTRLicensePlateReader_text = '".addslashes($LTRLicensePlateReader_text)."',
							WeatherStation = '".$WeatherStation."',
							WeatherStation_text = '".addslashes($WeatherStation_text)."',
							PeopleCounter = '".$PeopleCounter."',
							PeopleCounter_text = '".addslashes($PeopleCounter_text)."',
							CarCounter = '".$CarCounter."',
							CarCounter_text = '".addslashes($CarCounter_text)."',
							EVCharger = '".$EVCharger."',
							EVCharger_text = '".addslashes($EVCharger_text)."',
							RoadTempMonitor = '".$RoadTempMonitor."',
							RoadTempMonitor_text = '".addslashes($RoadTempMonitor_text)."',
							LEDDisplay = '".$LEDDisplay."',
							LEDDisplay_text = '".addslashes($LEDDisplay_text)."',
							SmallCellularAntenna = '".$SmallCellularAntenna."',
							SmallCellularAntenna_text = '".addslashes($SmallCellularAntenna_text)."',
							FirstResponderAntenna = '".$FirstResponderAntenna."',
							FirstResponderAntenna_text = '".addslashes($FirstResponderAntenna_text)."',
							PowerMeter = '".$PowerMeter."',
							PowerMeter_text = '".addslashes($PowerMeter_text)."',
							Other = '".$Other."',
							node_type = '".addslashes($node_type)."',
							Other_text = '".addslashes($Other_text)."'".$assets_value."
							WHERE mac_address = '".$mac_address."'";
						$update = $db->query($sql);

						$sqll 	=	"SELECT ID FROM slc_installation_data WHERE slc_id = '".$slc_id."' LIMIT 1";
						$stmt	=	$db->query($sqll);
						$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
						if(isset($data[0]->ID)){
							$synch = AutoSyncSLCStatus($data[0]->ID);
						}

						$msg 	= "Your data is saved. You will be able to add/edit this data later from List -> Edit";
						$success = '';
						$success .= "$msg";
						echo json_encode(array("status" => 'success','msg'=>$success));
					}
					else{
						if(isset($_FILES['pole_image']['name']) && $_FILES['pole_image']['name']!=""){
							$pole_image		=	uploadPoleImg($slc_id,$_FILES);
						}
						if(isset($assets)){
							$assets = getConvertAssets($assets);
						}

						//$assets_key = '';
						//$assets_value = '';
						//if($skip == 'No'){
							$assets_key = ', assets';
							$assets_value = ", '".addslashes($assets)."'";
						/*}else{
							$assets_key = '';
							$assets_value = '';
						}*/
						$sql = "INSERT INTO slc_installation_data (client_id,
												  user_id,
												  mac_address,
												  slc_id,
												  lat,
												  lng,
												  address,
												  measurement_units,
												  pole_id,
												  pole_image,
												  date_of_installation,
												  created,
												  created_by,
												  ipaddress,
												  source,
												  notes,
												  Camera,
												  Camera_text,
												  AirQualityMonitor,
												  AirQualityMonitor_text,
												  Microphone,
												  Microphone_text,
												  Radar,
												  Radar_text,
												  LTRLicensePlateReader,
												  LTRLicensePlateReader_text,
												  WeatherStation,
												  WeatherStation_text,
												  PeopleCounter,
												  PeopleCounter_text,
												  CarCounter,
												  CarCounter_text,
												  EVCharger,
												  EVCharger_text,
												  RoadTempMonitor,
												  RoadTempMonitor_text,
												  LEDDisplay,
												  LEDDisplay_text,
												  SmallCellularAntenna,
												  SmallCellularAntenna_text,
												  FirstResponderAntenna,
												  FirstResponderAntenna_text,
												  PowerMeter,
												  PowerMeter_text,
												  Other,
												  Other_text, node_type".$assets_key.")
												  VALUES
												  (".$client_id.",
												  ".$user_id.",
												  '".addslashes($mac_address)."',
												  '".addslashes($slc_id)."',
												  '".addslashes($lat)."',
												  '".addslashes($lng)."',
												  '".addslashes($address)."',
												  '".addslashes($measurement_units)."',
												  '".addslashes($pole_id)."',
												  '".addslashes($pole_image)."',
												  '".addslashes($date_of_installation)."',
												  '".addslashes($created)."',
												  ".$user_id.",
												  '".$ipaddress."',
												  '".$source."', 
												  '".addslashes($notes)."',
												  '".$Camera."',
												  '".addslashes($Camera_text)."',
												  '".$AirQualityMonitor."',
												  '".addslashes($AirQualityMonitor_text)."',
												  '".$Microphone."',
												  '".addslashes($Microphone_text)."',
												  '".$Radar."',
												  '".addslashes($Radar_text)."',
												  '".$LTRLicensePlateReader."',
												  '".addslashes($LTRLicensePlateReader_text)."',
												  '".$WeatherStation."',
												  '".addslashes($WeatherStation_text)."',
												  '".$PeopleCounter."',
												  '".addslashes($PeopleCounter_text)."',
												  '".$CarCounter."',
												  '".addslashes($CarCounter_text)."',
												  '".$EVCharger."',
												  '".addslashes($EVCharger_text)."',
												  '".$RoadTempMonitor."',
												  '".addslashes($RoadTempMonitor_text)."',
												  '".$LEDDisplay."',
												  '".addslashes($LEDDisplay_text)."',
												  '".$SmallCellularAntenna."',
												  '".addslashes($SmallCellularAntenna_text)."',
												  '".$FirstResponderAntenna."',
												  '".addslashes($FirstResponderAntenna_text)."',
												  '".$PowerMeter."',
												  '".addslashes($PowerMeter_text)."',
												  '".$Other."',
												  '".addslashes($Other_text)."',
												  '".$node_type."' ".$assets_value."
												 )";
						$stmt 				= $db->query($sql);
						$lastInsertId 		= $db->lastInsertId();
						$logs = slcLog($lastInsertId,$data);					
						if($clientinfo->AutoSync == 'Yes'){
							$synch = AutoSyncSLCStatus($lastInsertId);
						}
						
						if($skip == 'No'){				
							$success = $lvars['SLC_DATA_SAVED'];
						}
						else{
							$success = $lvars['SLC_DATA_SKIP'];
						}
						echo json_encode(array("status" => 'success','msg'=>$success));
					}
				}
			}
			else{
				echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
			}
		}else{
			echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID']." ".$lvars['RULE_26']));
		}
	}
	else{	
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
	}
    exit();
}
function AutoSyncSLCStatus($ID)
{
	global $cnf;
	$db 					= 	getConnection();
	$data					=	getSLCInfo($ID);
	$cid					=	$data->client_id;
	$source 				=	$data->source;
	$ipaddress				= 	$_SERVER['REMOTE_ADDR'];
	$createdby				=	$data->client_id;
	
	//update status
	$sql 			= 	"UPDATE slc_installation_data SET isApproved='Yes',sync='Process' WHERE id=".$ID;
	$update 		= 	$db->query($sql);
	
	//insert log
	$sql 				= "INSERT INTO slc_approve (slcid,clientid,source,ipaddress,created_by) VALUES (".$ID.",".$cid.",'Web','".addslashes($ipaddress)."',".$createdby.");";
	$result 			= $db->query($sql);
	
	//insert log
	$sql 				= "INSERT INTO slc_sync (clientid,slcid,ipaddress,created_by) VALUES (".$cid.",".$ID.",'".addslashes($ipaddress)."',".$createdby.");";
	$result 			= $db->query($sql);
	
	return true;
}
function slcLog($ID,$data)
{
	global $cnf;
	$db 					= 	getConnection();
	$oldData				=	getSLCInfo($ID);
	$client_id				=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:$oldData->client_id;
	$user_id				=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:$oldData->user_id;
	$mac_address			=	isset($data['mac_address']) && !empty($data['mac_address']) ? $data['mac_address']:$oldData->mac_address;
	$slc_id					=	isset($data['slc_id']) && !empty($data['slc_id']) ? $data['slc_id']:$oldData->slc_id;
	$lat					=	isset($data['lat']) && !empty($data['lat']) ? $data['lat']:$oldData->lat;
	$lng					=	isset($data['lng']) && !empty($data['lng']) ? $data['lng']:$oldData->lng;
	$address				=	isset($data['address']) && !empty($data['address']) ? $data['address']:$oldData->address;
	$measurement_units		=	isset($data['measurement_units']) && !empty($data['measurement_units']) ? $data['measurement_units']:$oldData->measurement_units;
	$pole_id				=	isset($data['pole_id']) && !empty($data['pole_id']) ? $data['pole_id']:$oldData->pole_id;
	$assets					=	isset($data['Assets']) && !empty($data['Assets']) ? $data['Assets']:$oldData->assets;
	$date_of_installation	=	isset($data['date_of_installation']) && !empty($data['date_of_installation']) ? dateConvertToYMD($data['date_of_installation']):$oldData->date_of_installation;
	$source 				=	isset($data['source'])?$data['source']:$oldData->source;
	$pole_image 			=	isset($oldData->pole_image) && !empty($oldData->pole_image) ? $oldData->pole_image : "";
	$ipaddress				= 	$_SERVER['REMOTE_ADDR'];
	//check code
	$sql 				= "INSERT INTO slc_installation_data_logs (slcd_id,
													  client_id,
													  user_id,
													  mac_address,
													  slc_id,
													  lat,
													  lng,
													  address,
													  measurement_units,
													  pole_id,
													  pole_image,
													  assets,
													  date_of_installation,
													  created_by,
													  ipaddress,
													  source)
													  VALUES
													  (".$ID.",
													  ".$client_id.",
													  ".$user_id.",
													  '".addslashes($mac_address)."',
													  '".addslashes($slc_id)."',
													  '".addslashes($lat)."',
													  '".addslashes($lng)."',
													  '".addslashes($address)."',
													  '".addslashes($measurement_units)."',
													  '".addslashes($pole_id)."',
													  '".addslashes($pole_image)."',
													  '".addslashes($assets)."',
													  '".addslashes($date_of_installation)."',
													  ".$user_id.",
													  '".$ipaddress."',
													  '".$source."'
													 )";
	$stmt = $db->query($sql);
	return true;
}
function getSLCInfo($ID)
{
	global $cnf;
	$db 	= 	getConnection();
	$sql 	=	"SELECT a.* FROM slc_installation_data AS a where a.ID = ".$ID;
	$stmt	=	$db->query($sql);
	$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	if(isset($data[0]->pole_image) && !empty($data[0]->pole_image))
	{
		$data[0]->pole_image_url = IMG_URL.$data[0]->pole_image;
	}
	else
	{
		$data[0]->pole_image_url = IMG_URL.'noimage.png';
	}
	return $data[0];
}
function getSLC($ID,$mu="")
{
	global $cnf;
	$db 	= 	getConnection();
	$sql 	=	"SELECT a.*, su.language FROM slc_installation_data AS a LEFT JOIN slc_users su ON a.user_id = su.id where a.ID = ".$ID;
	$stmt	=	$db->query($sql);
	$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	$data[0]->date_of_installation = date('m-d-Y',strtotime($data[0]->date_of_installation));
	$NewAssets = getClientAssets($data[0]->client_id,$data[0]->measurement_units,$data[0]->assets);

	if(isset($data[0]->pole_image) && !empty($data[0]->pole_image)){
		$data[0]->pole_image_url = IMG_URL.$data[0]->pole_image;
	}
	else{
		$data[0]->pole_image_url = IMG_URL.'noimage.png';
	}

	//get language
	$lvars		=	getLanguage($data[0]->user_id,'None');
	$msg = "";
	if(isset($mu) && isset($data[0]->measurement_units) && $data[0]->measurement_units != 'None')
	{
		if($mu != $data[0]->measurement_units)
		{
			$msg = $lvars['NOTE_MEASUREMENT_UNIT'];
		}
	}

	$resultData = array();
	$resultData['ID'] = $data[0]->ID;
	$resultData['client_id'] = $data[0]->client_id;
	$resultData['user_id'] = $data[0]->user_id;
	if($data[0]->internal_mac_address != ''){
		$resultData['mac_address_type'] = 'internal';
		$resultData['mac_address'] = $data[0]->internal_mac_address;
		$resultData['slc_id'] = $data[0]->internal_slc_id;
	}else{
		$resultData['mac_address_type'] = 'external';
		$resultData['mac_address'] = $data[0]->mac_address;
		$resultData['slc_id'] = $data[0]->slc_id;
	}
	
	$resultData['lat'] = $data[0]->lat;
	$resultData['lng'] = $data[0]->lng;
	$resultData['address'] = $data[0]->address;
	$resultData['measurement_units'] = $data[0]->measurement_units;
	$resultData['pole_id'] = $data[0]->pole_id;
	$resultData['pole_image'] = $data[0]->pole_image;
	$resultData['pole_image_url'] = $data[0]->pole_image_url;
	$resultData['date_of_installation'] = $data[0]->date_of_installation;
	$resultData['notes'] = $data[0]->notes;
	$resultData['node_type'] = $data[0]->node_type;
	$resultData['language'] = $data[0]->language;

	$resultData['pole_options'][0]['key'] 		= 'Camera';
	$resultData['pole_options'][0]['value'] 	= 'Camera';
	$resultData['pole_options'][0]['Camera'] 	= $data[0]->Camera;
	$resultData['pole_options'][0]['Camera_text'] 	= $data[0]->Camera_text;

	$resultData['pole_options'][1]['key'] 		= 'AirQualityMonitor';
	$resultData['pole_options'][1]['value'] 	= 'AirQuality Monitor';
	$resultData['pole_options'][1]['AirQualityMonitor'] 	= $data[0]->AirQualityMonitor;
	$resultData['pole_options'][1]['AirQualityMonitor_text'] 	= $data[0]->AirQualityMonitor_text;

	$resultData['pole_options'][2]['key'] 		= 'Microphone';
	$resultData['pole_options'][2]['value'] 	= 'Microphone';
	$resultData['pole_options'][2]['Microphone'] 	= $data[0]->Microphone;
	$resultData['pole_options'][2]['Microphone_text'] 	= $data[0]->Microphone_text;

	$resultData['pole_options'][3]['key'] 		= 'Radar';
	$resultData['pole_options'][3]['value'] 	= 'Radar (Speed)';
	$resultData['pole_options'][3]['Radar'] 	= $data[0]->Radar;
	$resultData['pole_options'][3]['Radar_text'] 	= $data[0]->Radar_text;

	$resultData['pole_options'][4]['key'] 		= 'LTRLicensePlateReader';
	$resultData['pole_options'][4]['value'] 	= 'LTR License Plate Reader';
	$resultData['pole_options'][4]['LTRLicensePlateReader'] 	= $data[0]->LTRLicensePlateReader;
	$resultData['pole_options'][4]['LTRLicensePlateReader_text'] 	= $data[0]->LTRLicensePlateReader_text;

	$resultData['pole_options'][5]['key'] 		= 'WeatherStation';
	$resultData['pole_options'][5]['value'] 	= 'Weather Station';
	$resultData['pole_options'][5]['WeatherStation'] 	= $data[0]->WeatherStation;
	$resultData['pole_options'][5]['WeatherStation_text'] 	= $data[0]->WeatherStation_text;

	$resultData['pole_options'][6]['key'] 		= 'PeopleCounter';
	$resultData['pole_options'][6]['value'] 	= 'People Counter';
	$resultData['pole_options'][6]['PeopleCounter'] 	= $data[0]->PeopleCounter;
	$resultData['pole_options'][6]['PeopleCounter_text'] 	= $data[0]->PeopleCounter_text;

	$resultData['pole_options'][7]['key'] 		= 'CarCounter';
	$resultData['pole_options'][7]['value'] 	= 'Car Counter';
	$resultData['pole_options'][7]['CarCounter'] 	= $data[0]->CarCounter;
	$resultData['pole_options'][7]['CarCounter_text'] 	= $data[0]->CarCounter_text;

	$resultData['pole_options'][8]['key'] 		= 'EVCharger';
	$resultData['pole_options'][8]['value'] 	= 'EV Charger';
	$resultData['pole_options'][8]['EVCharger'] 	= $data[0]->EVCharger;
	$resultData['pole_options'][8]['EVCharger_text'] 	= $data[0]->EVCharger_text;

	$resultData['pole_options'][9]['key'] 		= 'RoadTempMonitor';
	$resultData['pole_options'][9]['value'] 	= 'Road Temp Monitor';
	$resultData['pole_options'][9]['RoadTempMonitor'] 	= $data[0]->RoadTempMonitor;
	$resultData['pole_options'][9]['RoadTempMonitor_text'] 	= $data[0]->RoadTempMonitor_text;

	$resultData['pole_options'][10]['key'] 	= 'LEDDisplay';
	$resultData['pole_options'][10]['value'] 	= 'LED Display';
	$resultData['pole_options'][10]['LEDDisplay'] 	= $data[0]->LEDDisplay;
	$resultData['pole_options'][10]['LEDDisplay_text'] 	= $data[0]->LEDDisplay_text;

	$resultData['pole_options'][11]['key'] 	= 'SmallCellularAntenna';
	$resultData['pole_options'][11]['value'] 	= 'Small Cellular Antenna';
	$resultData['pole_options'][11]['SmallCellularAntenna'] 	= $data[0]->SmallCellularAntenna;
	$resultData['pole_options'][11]['SmallCellularAntenna_text'] 	= $data[0]->SmallCellularAntenna_text;

	$resultData['pole_options'][12]['key'] 	= 'FirstResponderAntenna';
	$resultData['pole_options'][12]['value'] 	= 'First Responder Antenna';
	$resultData['pole_options'][12]['FirstResponderAntenna'] 	= $data[0]->FirstResponderAntenna;
	$resultData['pole_options'][12]['FirstResponderAntenna_text'] 	= $data[0]->FirstResponderAntenna_text;

	$resultData['pole_options'][13]['key'] 	= 'PowerMeter';
	$resultData['pole_options'][13]['value'] 	= 'Power Meter';
	$resultData['pole_options'][13]['PowerMeter'] 	= $data[0]->PowerMeter;
	$resultData['pole_options'][13]['PowerMeter_text'] 	= $data[0]->PowerMeter_text;

	$resultData['pole_options'][14]['key'] 	= 'Other';
	$resultData['pole_options'][14]['value'] 	= 'Other';
	$resultData['pole_options'][14]['Other'] 	= $data[0]->Other;
	$resultData['pole_options'][14]['Other_text'] 	= $data[0]->Other_text;

	//echo "<pre>";print_r($NewAssets);exit;
	$setting	=	getSetting($data[0]->client_id,'detail');
	if($setting['client_slc_pole_assets_view'] == 'Yes'){
		$resultData['Assets'] = $NewAssets;
	}else{
		$resultData['Assets'] = [];
	}
	//echo "<pre>";print_r($PoleOptions);exit;

	echo json_encode(array("status" => 'success','data'=>$resultData,'msg'=>$msg)); 
	exit();
}

function getSlcList($UserID,$PageNo,$lat,$lon)
{
	global $cnf;
	$data			= 	Slim::getInstance()->request()->post();
	$search			=	isset($data['search'])?$data['search']:"";
	$node_type		=	isset($data['node_type'])?$data['node_type']:"";
	//$lat			=	isset($data['lat'])?$data['lat']:"";
	//$lon			=	isset($data['lon'])?$data['lon']:"";
	$db 			= 	getConnection();
	$userInfo 		=	getUserInfo($UserID);
	$lvars			=	getLanguage($UserID,'None');
	$clientID		=	$userInfo->client_id;
	$clientinfo 	= 	getClientInfo($clientID);
	$setting		=	getSetting($clientID,'detail');
	$conditions = '';

	if(isset($clientinfo) && !empty($clientinfo))
	{
		$limit = 30;
		$page = isset($PageNo) ? $PageNo : 0;
		if($page){
			$start = ($page - 1) * $limit;
		}
		else{
			$start = 0;
		}

		$R = 3960;  // earth's mean radius
		$rad = '100';
		// first-cut bounding box (in degrees)
		$maxLat = $lat + rad2deg($rad/$R);
		$minLat = $lat - rad2deg($rad/$R);
		// compensate for degrees longitude getting smaller with increasing latitude
		$maxLon = $lon + rad2deg($rad/$R/cos(deg2rad($lat)));
		$minLon = $lon - rad2deg($rad/$R/cos(deg2rad($lat)));

		$maxLat=number_format((float)$maxLat, 6, '.', '');
		$minLat=number_format((float)$minLat, 6, '.', '');
		$maxLon=number_format((float)$maxLon, 6, '.', '');
		$minLon=number_format((float)$minLon, 6, '.', '');

		$sql 	=	"SELECT vShowAllSLC, clientid, hosted_server FROM slc_clients WHERE id = ".$clientID." LIMIT 1";
		$stmt	=	$db->query($sql);
		$dataa	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($dataa);exit;
		if($dataa[0]->vShowAllSLC == 'No'){
			if(!empty($search)){
				$conditions .= " AND (a.slc_id like '%".$search."%' || a.internal_slc_id like '%".$search."%') ";
			}
			if(!empty($node_type)){
				$conditions .= " AND a.node_type like '%".$node_type."%' ";
			}
			
			$sql1 = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created, a.node_type, a.address FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.client_id = ".$clientID." ".$conditions." AND a.user_id in (select id from slc_users where client_id = ".$clientID." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' ORDER BY a.ID DESC";
			$stmt1	= $db->query($sql1);
			$data1	= $stmt1->fetchAll(PDO::FETCH_OBJ);
			$cnt 	= count($data1);

			$sql = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created, a.node_type, a.address FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.client_id = ".$clientID." AND address != '' ".$conditions." AND a.user_id in (select id from slc_users where client_id = ".$clientID." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' ORDER BY (POW((a.lng-'".$lon."'),2) + POW((a.lat-'".$lat."'),2)) LIMIT $start, $limit";
		}else{
			if(!empty($search)){
				//$conditions .= " AND (a.mac_address like '%".$search."%' || a.slc_id like '%".$search."%' || a.internal_mac_address like '%".$search."%' || a.internal_slc_id like '%".$search."%' || a.pole_id like '%".$search."%') ";
				$conditions .= " AND (a.slc_id like '%".$search."%' || a.internal_slc_id like '%".$search."%') ";
			}
			if(!empty($node_type)){
				$conditions .= " AND a.node_type like '%".$node_type."%' ";
			}

			$sql1 = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created, a.node_type FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.client_id = ".$clientID." ".$conditions." AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' ORDER BY a.ID DESC";
			$stmt1	= $db->query($sql1);
			$data1	= $stmt1->fetchAll(PDO::FETCH_OBJ);
			//echo "<pre>";print_r($data1);exit;
			$cnt 	= count($data1);

			$sql = "SELECT a.ID, a.mac_address, a.slc_id, a.pole_id, a.lat, a.lng, a.internal_mac_address, a.internal_slc_id, a.created, a.node_type, a.address FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.client_id = ".$clientID." AND address != '' ".$conditions." AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' ORDER BY (POW((a.lng-'".$lon."'),2) + POW((a.lat-'".$lat."'),2)) LIMIT $start, $limit";
		}
		$stmt	=	$db->query($sql);
		$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($data);exit;

		if($cnt > 0)
		{
			foreach($data as $key=>$val)
			{
				if($val->internal_mac_address != ''){
					$data[$key]->mac_address = $data[$key]->internal_mac_address;
					$data[$key]->slc_id = $data[$key]->internal_slc_id;
					$data[$key]->mac_address_type = 'internal';
				}else{
					$data[$key]->mac_address = $data[$key]->mac_address;
					$data[$key]->slc_id = $data[$key]->slc_id;
					$data[$key]->mac_address_type = 'external';
				}

				if(empty($val->mac_address))
				{
					$data[$key]->mac_address = "None";
					$data[$key]->pole_id = "None";
					$data[$key]->lat = "0.0000";
					$data[$key]->lng = "0.0000";
					$data[$key]->address = "None";
				}
				$data[$key]->created = date('m/d/Y h:i A', strtotime($val->created));
				$data[$key]->node_type = $val->node_type;
			}
			$result_arr['List'] = $data;
			$result_arr['tot_no_of_records'] = "$cnt";
			echo json_encode(array("status" => '1','data'=>$result_arr,"setting"=>$setting)); 
		}
		else
		{
			$result_arr['List'] = [];
			if(!empty($search)){
				echo json_encode(array("status" => 'error','data'=>$result_arr,'msg'=>$lvars['SLCs_NOT_FOUND'],"setting"=>$setting)); 
			}else{
				echo json_encode(array("status" => 'error','data'=>$result_arr,'msg'=>$lvars['SLC_NOT_FOUND'],"setting"=>$setting));
			}
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}
	exit();
}

/*function getSlcList($UserID,$PageNo)
{
	global $cnf;
	$data			= 	Slim::getInstance()->request()->post();
	$search			=	isset($data['search'])?$data['search']:"";
	$db 			= 	getConnection();
	$userInfo 		=	getUserInfo($UserID);
	$lvars			=	getLanguage($UserID,'None');
	$clientID		=	$userInfo->client_id;
	$clientinfo 	= 	getClientInfo($clientID);
	$setting		=	getSetting($clientID,'detail');
	$conditions = '';

	if(isset($clientinfo) && !empty($clientinfo))
	{
		$limit = 30;
		$page = isset($PageNo) ? $PageNo : 0;
		if($page){
			$start = ($page - 1) * $limit;
		}
		else{
			$start = 0;
		}

		$sql 	=	"SELECT vShowAllSLC FROM slc_clients WHERE id = ".$clientID." LIMIT 1";
		$stmt	=	$db->query($sql);
		$dataa	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($dataa);exit;
		if($dataa[0]->vShowAllSLC == 'No'){
			if(!empty($search)){
				$conditions .= " AND (a.mac_address like '%".$search."%' || a.slc_id like '%".$search."%' || a.internal_mac_address like '%".$search."%' || a.internal_slc_id like '%".$search."%' || a.pole_id like '%".$search."%') ";
			}
			
			$sql1 = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created FROM slc_installation_data AS a WHERE a.client_id = ".$clientID." ".$conditions." AND a.user_id in (select id from slc_users where client_id = ".$clientID." AND username = '".$userInfo->username."') ORDER BY a.ID DESC";
			$stmt1	= $db->query($sql1);
			$data1	= $stmt1->fetchAll(PDO::FETCH_OBJ);
			$cnt 	= count($data1);

			$sql = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created FROM slc_installation_data AS a WHERE a.client_id = ".$clientID." AND address != '' ".$conditions." AND a.user_id in (select id from slc_users where client_id = ".$clientID." AND username = '".$userInfo->username."') ORDER BY a.ID DESC LIMIT $start, $limit";
		}else{
			if(!empty($search)){
				$conditions .= " AND (a.mac_address like '%".$search."%' || a.slc_id like '%".$search."%' || a.internal_mac_address like '%".$search."%' || a.internal_slc_id like '%".$search."%' || a.pole_id like '%".$search."%') ";
			}

			$sql1 = "SELECT a.ID,a.mac_address,a.slc_id,a.pole_id,a.lat,a.lng, a.internal_mac_address, a.internal_slc_id, a.created FROM slc_installation_data AS a WHERE a.client_id = ".$clientID." ".$conditions." ORDER BY a.ID DESC";
			$stmt1	= $db->query($sql1);
			$data1	= $stmt1->fetchAll(PDO::FETCH_OBJ);
			//echo "<pre>";print_r($data1);exit;
			$cnt 	= count($data1);

			$sql = "SELECT a.ID, a.mac_address, a.slc_id, a.pole_id, a.lat, a.lng, a.internal_mac_address, a.internal_slc_id, a.created FROM slc_installation_data AS a WHERE a.client_id = ".$clientID." AND address != '' ".$conditions." ORDER BY a.ID DESC LIMIT $start, $limit";
		}
		$stmt	=	$db->query($sql);
		$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($data);exit;

		if($cnt > 0)
		{
			foreach($data as $key=>$val)
			{
				if($val->internal_mac_address != ''){
					$data[$key]->mac_address = $data[$key]->internal_mac_address;
					$data[$key]->slc_id = $data[$key]->internal_slc_id;
					$data[$key]->mac_address_type = 'internal';
				}else{
					$data[$key]->mac_address = $data[$key]->mac_address;
					$data[$key]->slc_id = $data[$key]->slc_id;
					$data[$key]->mac_address_type = 'external';
				}

				if(empty($val->mac_address))
				{
					$data[$key]->mac_address = "None";
					$data[$key]->pole_id = "None";
					$data[$key]->lat = "0.0000";
					$data[$key]->lng = "0.0000";
					$data[$key]->address = "None";
				}
				$data[$key]->created = date('m/d/Y h:i A', strtotime($val->created));
			}
			$result_arr['List'] = $data;
			$result_arr['tot_no_of_records'] = "$cnt";
			echo json_encode(array("status" => '1','data'=>$result_arr,"setting"=>$setting)); 

			//echo json_encode(array("status" => 'success','data'=>$data,"setting"=>$setting)); 
		}
		else
		{
			$result_arr['List'] = [];
			echo json_encode(array("status" => 'error','data'=>$result_arr,'msg'=>$lvars['SLC_NOT_FOUND'],"setting"=>$setting)); 
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}
	exit();
}*/
function getSlcListUsingLatLong()
{
	global $cnf;
	$data			= Slim::getInstance()->request()->post();
	$db 			= 	getConnection();

	/*$latitude		= isset($data['latitude'])?$data['latitude']:"";
	$longitude		= isset($data['longitude'])?$data['longitude']:"";
	$kilometer		= isset($data['kilometer'])?$data['kilometer']:"";*/

	$top_lat		= isset($data['top_lat'])?$data['top_lat']:"";
	$top_lon		= isset($data['top_lon'])?$data['top_lon']:"";
	$bottom_lat		= isset($data['bottom_lat'])?$data['bottom_lat']:"";
	$bottom_lon		= isset($data['bottom_lon'])?$data['bottom_lon']:"";
	$clientid		= isset($data['clientid'])?$data['clientid']:"";
	$userid			= isset($data['userid'])?$data['userid']:"0";

	//if(isset($latitude) && $latitude != '' && isset($longitude) && $longitude != '' && isset($kilometer) && $kilometer != '' && isset($clientid) && $clientid != ''){
	if(isset($top_lat) && $top_lat != '' && isset($top_lon) && $top_lon != '' && isset($bottom_lat) && $bottom_lat != '' && isset($bottom_lon) && $bottom_lon != '' && isset($clientid) && $clientid != ''){
		$sql 	=	"SELECT vShowAllSLC, clientid, hosted_server FROM slc_clients WHERE id = ".$clientid." LIMIT 1";
		$stmt	=	$db->query($sql);
		$dataa	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($dataa);exit;
		if($dataa[0]->vShowAllSLC == 'No'){
			if($userid > 0){
				$userInfo = getUserInfo($userid);
				$sql = "SELECT a.ID, a.pole_id, a.slc_id, a.lat, a.lng FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE ('".$top_lat."' < '".$bottom_lat."' AND lat BETWEEN '".$top_lat."' AND '".$bottom_lat."') OR ('".$bottom_lat."' < '".$top_lat."' AND lat BETWEEN '".$bottom_lat."' AND '".$top_lat."') AND ('".$top_lon."' < '".$bottom_lon."' AND lng BETWEEN '".$top_lon."' AND '".$bottom_lon."') OR ('".$bottom_lon."' < '".$top_lon."' AND lng BETWEEN '".$bottom_lon."' AND '".$top_lon."') AND a.client_id = ".$clientid." AND a.user_id in (select id from slc_users where client_id = ".$clientID." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."'";
			}else{
				$sql = "SELECT a.ID, a.pole_id, a.slc_id, a.lat, a.lng FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE ('".$top_lat."' < '".$bottom_lat."' AND lat BETWEEN '".$top_lat."' AND '".$bottom_lat."') OR ('".$bottom_lat."' < '".$top_lat."' AND lat BETWEEN '".$bottom_lat."' AND '".$top_lat."') AND ('".$top_lon."' < '".$bottom_lon."' AND lng BETWEEN '".$top_lon."' AND '".$bottom_lon."') OR ('".$bottom_lon."' < '".$top_lon."' AND lng BETWEEN '".$bottom_lon."' AND '".$top_lon."') AND a.client_id = ".$clientid." AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."'";
			}
		}else{
			if($userid > 0){
				$userInfo = getUserInfo($userid);
				$sql = "SELECT a.ID, a.pole_id, a.slc_id, a.lat, a.lng FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE ('".$top_lat."' < '".$bottom_lat."' AND lat BETWEEN '".$top_lat."' AND '".$bottom_lat."') OR ('".$bottom_lat."' < '".$top_lat."' AND lat BETWEEN '".$bottom_lat."' AND '".$top_lat."') AND ('".$top_lon."' < '".$bottom_lon."' AND lng BETWEEN '".$top_lon."' AND '".$bottom_lon."') OR ('".$bottom_lon."' < '".$top_lon."' AND lng BETWEEN '".$bottom_lon."' AND '".$top_lon."') AND a.client_id = ".$clientid." AND address != ''  AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."'";
			}else{
				$sql = "SELECT a.ID, a.pole_id, a.slc_id, a.lat, a.lng FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE ('".$top_lat."' < '".$bottom_lat."' AND lat BETWEEN '".$top_lat."' AND '".$bottom_lat."') OR ('".$bottom_lat."' < '".$top_lat."' AND lat BETWEEN '".$bottom_lat."' AND '".$top_lat."') AND ('".$top_lon."' < '".$bottom_lon."' AND lng BETWEEN '".$top_lon."' AND '".$bottom_lon."') OR ('".$bottom_lon."' < '".$top_lon."' AND lng BETWEEN '".$bottom_lon."' AND '".$top_lon."') AND a.client_id = ".$clientID." AND address != '' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."'";
			}
		}
		$stmt	= $db->query($sql);
		$data	= $stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($data);exit;
		$cnt 	= count($data);
		if($cnt > 0)
		{
			$result_arr['tot_no_of_records'] = "$cnt";
			$result_arr['List'] = $data;
			echo json_encode(array("status" => '1','data'=>$result_arr));
		} 
		else
		{
			echo json_encode(array("status" => 'error','msg'=>"No SLCs Found")); 
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>"Invalid Parameters"));
	}
}
function getSettingList($client_id,$checkAssetsName,$checkAttributeName,$mu='None')
{
	$db = getConnection();

	$clientinfo 	= 	getClientInfo($client_id);
	//echo "<pre>";print_r($clientinfo);exit;

	$sql = "SELECT a.assets FROM slc_setting as a where a.client_id =".$client_id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	$options = array();
	if(isset($result[0]->assets))
	{
		$assets = json_decode($result[0]->assets);
		if($clientinfo->vLGVersion == 'LG5'){
			if(isset($assets->assets) && count($assets->assets) > 0)
			{
				$cnt = 0;
				foreach($assets->assets as $val)
				{
					if(trim($val->assetName) == trim($checkAssetsName))
					{
						$AssetName = $val->assetName;
						$AssetsKey = trim($AssetName);
						//$AssetsKey = str_replace(" ","_",$AssetsKey);
						if(isset($val->attributes) && count($val->attributes) > 0)
						{
							foreach($val->attributes as $sval)
							{
								if(trim($sval->attributeName) == trim($checkAttributeName))
								{
									$AttrKey = trim($sval->attributeName);
									//$AttrKey = str_replace(" ","_",$AttrKey);
									$AttrKey = $AssetsKey.'-k-'.$AttrKey;
									$valOptions = array();
									if(isset($sval->values) && count($sval->values) > 0 && (trim($sval->attributeName) == 'Pole Height' || trim($sval->attributeName) == 'Arm Length' || trim($sval->attributeName) == 'Diameter'))
									{
										if(trim($sval->attributeName) == 'Pole Height')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->attributeName) == 'Arm Length')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->attributeName) == 'Diameter')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'inch':"cm";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
									}
									else
									{
										if(isset($sval->values) && count($sval->values) > 0)
										{
											foreach($sval->values as $vval)
											{
												$valOptions[] = $vval;
											}
										}
									}
									
									if(empty($valOptions))
									{
										$valOptions = $sval->values;
									}
									
									$Attlabel  		= $sval->attributeName;
									$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->attributeName , -1,  PREG_SPLIT_NO_EMPTY);
									$Note 			= 'Select '.$sval->attributeName;
									$BtnText 		= 'Select '.$sval->attributeName; 
									if(count($AttributeSplit) > 0)
									{
										$Attlabel 	= "";
										$Note 		= 'Select ';
										$BtnText 	= 'Select ';
										foreach($AttributeSplit as $val)
										{
											$Note .= $val.' ';
											$BtnText .= $val.' ';
											$Attlabel .= $val.'';
										}
										$Note 		= str_replace("_"," ",$Note);
										$BtnText 	= str_replace("_"," ",$BtnText);
										$Attlabel 	= str_replace("_"," ",$Attlabel);	
									}
									//sort array
									if(isset($valOptions) && count($valOptions) > 0)
									{
										$valOptions = sortArray($valOptions);
									}
									//return
									$options = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>'');
									$cnt++;
								}
							}
						}
					}
				}
			}
		}else{
			if(isset($assets->Assets) && count($assets->Assets) > 0)
			{
				$cnt = 0;
				foreach($assets->Assets as $val)
				{
					if(trim($val->AssetName) == trim($checkAssetsName))
					{
						$AssetName = $val->AssetName;
						$AssetsKey = trim($AssetName);
						//$AssetsKey = str_replace(" ","_",$AssetsKey);
						if(isset($val->Attributes) && count($val->Attributes) > 0)
						{
							foreach($val->Attributes as $sval)
							{
								if(trim($sval->AttributeName) == trim($checkAttributeName))
								{
									$AttrKey = trim($sval->AttributeName);
									//$AttrKey = str_replace(" ","_",$AttrKey);
									$AttrKey = $AssetsKey.'-k-'.$AttrKey;
									$valOptions = array();
									if(isset($sval->Values) && count($sval->Values) > 0 && (trim($sval->AttributeName) == 'PoleHeight' || trim($sval->AttributeName) == 'ArmLength' || trim($sval->AttributeName) == 'Diameter'))
									{
										if(trim($sval->AttributeName) == 'PoleHeight')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->AttributeName) == 'ArmLength')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->AttributeName) == 'Diameter')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'inch':"cm";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
									}
									else
									{
										if(isset($sval->Values) && count($sval->Values) > 0)
										{
											foreach($sval->Values as $vval)
											{
												$valOptions[] = $vval;
											}
										}
									}
									
									if(empty($valOptions))
									{
										$valOptions = $sval->Values;
									}
									
									$Attlabel  		= $sval->AttributeName;
									$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->AttributeName , -1,  PREG_SPLIT_NO_EMPTY);
									$Note 			= 'Select '.$sval->AttributeName;
									$BtnText 		= 'Select '.$sval->AttributeName; 
									if(count($AttributeSplit) > 0)
									{
										$Attlabel 	= "";
										$Note 		= 'Select ';
										$BtnText 	= 'Select ';
										foreach($AttributeSplit as $val)
										{
											$Note .= $val.' ';
											$BtnText .= $val.' ';
											$Attlabel .= $val.'';
										}
										$Note 		= str_replace("_"," ",$Note);
										$BtnText 	= str_replace("_"," ",$BtnText);
										$Attlabel 	= str_replace("_"," ",$Attlabel);	
									}
									//sort array
									if(isset($valOptions) && count($valOptions) > 0)
									{
										$valOptions = sortArray($valOptions);
									}
									//return
									$options = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>'');
									$cnt++;
								}
							}
						}
					}
				}
			}
		}
	}
	//response
	return $options;
}
function checkUniqueSLCID($client_id,$slc_id)
{
	$db = getConnection();
	$sql = "SELECT count(id) as tot FROM slc_installation_data where slc_id = '".$slc_id."' and client_id=".$client_id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	if(isset($result[0]->tot) && $result[0]->tot > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}
function checkUniqueSLCIDUniqueness($client_id,$slc_id,$user_id)
{
	$db = getConnection();
	$lvars			=	getLanguage($user_id,'None');
	$ipaddress	= 	$_SERVER['REMOTE_ADDR'];
	$sql = "SELECT count(id) as tot FROM slc_installation_data where slc_id = '".$slc_id."' and client_id=".$client_id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	$starttime = date('H:i:s');
	if(isset($result[0]->tot) && $result[0]->tot > 0)
	{
		return array("status" => 'success');
	}
	else
	{
		return array("status" =>'success');
	}
}
function checkUniqueMacAddress($mac_address)
{
	$db = getConnection();
	$sql = "SELECT count(id) as tot FROM slc_installation_data where mac_address = '".$mac_address."'";
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	if(isset($result[0]->tot) && $result[0]->tot > 0)
	{
		return true;
	}
	else
	{	
		return false;
	}
}
function checkInternalUniqueMacAddress($internal_mac_address)
{
	$db = getConnection();
	$sql = "SELECT count(id) as tot FROM slc_installation_data where internal_mac_address = '".$internal_mac_address."'";
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	if(isset($result[0]->tot) && $result[0]->tot > 0)
	{
		return true;
	}
	else
	{	
		return false;
	}
}
function checkInternalUniqueMacAddressAPI()
{
	$data			= Slim::getInstance()->request()->post();
	$db 			= getConnection();

	$user_id		= isset($data['user_id'])?$data['user_id']:"";
	$client_id		= isset($data['client_id'])?$data['client_id']:"";
	$mac_address	= isset($data['mac_address'])?$data['mac_address']:"";
	$source			= isset($data['source'])?$data['source']:"";
	$lvars			= getLanguage($user_id,'None');

	if(!empty($client_id) && !empty($mac_address) && ($source == 'IOS' || $source == 'Android'))
	{
		$sql = "SELECT count(id) as tot, internal_slc_id FROM slc_installation_data WHERE internal_mac_address = '".$mac_address."' AND client_id = '".$client_id."'";
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($result);exit;
		if(isset($result[0]->tot) && $result[0]->tot > 0)
		{
			$sql1 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE internal_mac_address = '".$mac_address."' AND client_id = '".$client_id."' AND address != '' LIMIT 1";
			$stmt1 		=	$db->query($sql1);
			$result1 	=	$stmt1->fetchAll(PDO::FETCH_OBJ);
			//echo "<pre>";print_r($result1);exit;
			if(isset($result1[0]->tot) && $result1[0]->tot > 0){
				echo json_encode(array("status" => 'continue', "msg" => $lvars['OVERWRTING_EXISTING_SLC'], 'mac_address_type' => 'internal', "slc_id" => $result[0]->internal_slc_id));
				$db = null;
				exit();
			}else{
				echo json_encode(array("status" => 'success', 'mac_address_type' => 'internal', "slc_id" => $result[0]->internal_slc_id));
				$db = null;
				exit();
			}

			/*echo json_encode(array("status" => 'internal', "slc_id" => $result[0]->internal_slc_id));
			$db = null;
			exit();*/
		}
		else
		{	
			echo json_encode(array("status" => 'success', 'mac_address_type' => 'external', "slc_id" => ''));
			$db = null;
			exit();
		}
	}else{	
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
		$db = null;
		exit();
	}
}
function checkInternalUniqueMacAddressEditAPI()
{
	$data			= Slim::getInstance()->request()->post();
	$db 			= getConnection();

	$user_id		= isset($data['user_id'])?$data['user_id']:"";
	$client_id		= isset($data['client_id'])?$data['client_id']:"";
	$mac_address	= isset($data['mac_address'])?$data['mac_address']:"";
	$source			= isset($data['source'])?$data['source']:"";
	$lvars			= getLanguage($user_id,'None');

	if(!empty($client_id) && !empty($mac_address) && ($source == 'IOS' || $source == 'Android'))
	{
		$sql = "SELECT count(id) as tot, internal_slc_id FROM slc_installation_data WHERE internal_mac_address = '".$mac_address."' AND client_id = '".$client_id."'";
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($result);exit;
		if(isset($result[0]->tot) && $result[0]->tot > 0)
		{
			echo json_encode(array("status" => 'internal', "slc_id" => $result[0]->internal_slc_id,'msg'=>'You are trying to update internal SLC. Please tap on Scan to install or update internal SLC.'));
			$db = null;
			exit();
		}
		else
		{
			$sql1 = "SELECT count(id) as tot, slc_id FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND client_id = '".$client_id."'";
			$stmt1 = $db->query($sql1);
			$result1 = $stmt1->fetchAll(PDO::FETCH_OBJ);
			if($result1[0]->slc_id != ''){
				$slc_id = $result1[0]->slc_id;
			}else{
				$slc_id = '';
			}
			echo json_encode(array("status" => 'external', "slc_id" => $slc_id));
			$db = null;
			exit();
		}
	}else{	
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
		$db = null;
		exit();
	}
}
function checkInternalUniqueSLCIDEditAPI()
{
	$data			= Slim::getInstance()->request()->post();
	$db 			= getConnection();

	$user_id		= isset($data['user_id'])?$data['user_id']:"";
	$client_id		= isset($data['client_id'])?$data['client_id']:"";
	$slc_id			= isset($data['slc_id'])?$data['slc_id']:"";
	$source			= isset($data['source'])?$data['source']:"";
	$lvars			= getLanguage($user_id,'None');

	if(!empty($client_id) && !empty($slc_id) && ($source == 'IOS' || $source == 'Android'))
	{
		$sql = "SELECT count(id) as tot, internal_mac_address FROM slc_installation_data WHERE internal_slc_id = '".$slc_id."' AND client_id = '".$client_id."'";
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($result);exit;
		if(isset($result[0]->tot) && $result[0]->tot > 0)
		{
			echo json_encode(array("status" => 'internal', "mac_address" => $result[0]->internal_mac_address,'msg'=>'You are trying to update internal SLC. Please tap on Scan to install or update internal SLC.'));
			$db = null;
			exit();
		}
		else
		{
			$sql1 = "SELECT count(id) as tot, mac_address FROM slc_installation_data WHERE slc_id = '".$slc_id."' AND client_id = '".$client_id."'";
			$stmt1 = $db->query($sql1);
			$result1 = $stmt1->fetchAll(PDO::FETCH_OBJ);
			if($result1[0]->mac_address != ''){
				$mac_address = $result1[0]->mac_address;
			}else{
				$mac_address = '';
			}
			echo json_encode(array("status" => 'external', "mac_address" => $mac_address));
			$db = null;
			exit();
		}
	}else{	
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
		$db = null;
		exit();
	}
}
function checkUniqueMacAddressUniqueness($mac_address,$client_id,$user_id)
{
	$db 		=	getConnection();
	$lvars		=	getLanguage($user_id,'None');
	$ipaddress	= 	$_SERVER['REMOTE_ADDR'];
	$sql 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND client_id = '".$client_id."' ";
	$stmt 		=	$db->query($sql);
	$result 	=	$stmt->fetchAll(PDO::FETCH_OBJ);
	$starttime 	=	date("H:i:s");
	$db = null;
	if(isset($result[0]->tot) && $result[0]->tot > 0)
	{
		return array("status" => 'success');
	}
	else
	{
		return array("status" =>'success');
	}
}
function checkMacAddressSlcIdbeforeSave()
{
	global $cnf;
	$data			= Slim::getInstance()->request()->post();
	$db 			= getConnection();

	$user_id		= isset($data['user_id'])?$data['user_id']:"";
	$client_id		= isset($data['client_id'])?$data['client_id']:"";
	$mac_address	= isset($data['mac_address'])?$data['mac_address']:"";
	$slc_id			= isset($data['slc_id'])?$data['slc_id']:"";
	$source			= isset($data['source'])?$data['source']:"";
	$node_type		= isset($data['node_type'])?$data['node_type']:"NBIOT";
	$lvars			= getLanguage($user_id,'None');
	$clientinfo 	= getClientInfo($client_id);
	$userInfo 		= getUserInfo($user_id);

	if(!empty($user_id) && !empty($client_id) && !empty($mac_address) && !empty($slc_id) && ($source == 'IOS' || $source == 'Android'))
	{
		if(!empty($mac_address)){
			$gump 		= new GUMP();
			$validdata 	= $gump->sanitize($data);
			if($clientinfo->typeid == 5)
			{
				$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
			}
			elseif($clientinfo->typeid == 4)
			{
				$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
			}elseif($clientinfo->typeid == 7)
			{
				$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15|mac_characters'));
			}
			elseif($clientinfo->typeid == 8)
			{
				if($clientinfo->iRPMA == '1' && strlen($mac_address) == 8){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
				}elseif($clientinfo->iOSDI == '1' && strlen($mac_address) == 11){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
				}else if($clientinfo->iNBIOT == '1' && strlen($mac_address) == 15){
					$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
				}else if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && strlen($mac_address) == 16){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
				}else{
					$gump->validation_rules(array('mac_address'=> 'required|invalid'));
				}

				if($clientinfo->iNBIOT == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
					$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
				}
				if($clientinfo->iRPMA == '1' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,8|mac_characters'));
				}
				if($clientinfo->iOSDI == '1' && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iZigbee == '0' && $clientinfo->iCisco == '0'){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,11|mac_characters'));
				}
				if(($clientinfo->iZigbee == '1' || $clientinfo->iCisco == '1') && $clientinfo->iRPMA == '0' && $clientinfo->iNBIOT == '0' && $clientinfo->iOSDI == '0'){
					$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
				}
				/*if($node_type == ''){
					if(strlen($mac_address) == 15){
						$gump->validation_rules(array('mac_address'=> 'required|numeric|min_len,15'));
					}else{
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|min_len,15|max_len,16|mac_characters'));
					}
				}
				if($node_type != ''){
					if($node_type == 'NBIOT'){
						$gump->validation_rules(array('mac_address'=> 'required|numeric|len_equal,15'));
					}else{
						$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
					}
				}*/
			}
			else
			{
				$gump->validation_rules(array('mac_address'=> 'required|alpha_numeric|len_equal,16|mac_characters'));
			}
			$gump->filter_rules(array('mac_address'=> 'trim'));
			$validated_data = $gump->run($validdata);
			if($validated_data === false) 
			{
				$errors = $gump->get_readable_errors(true, '', '', $user_id);
				$errors = str_replace("field",$clientinfo->typeLabel,$errors);
				echo json_encode(array("status" => 'error',"msg" => $errors));
				exit();
			}
		}

		if(!empty($slc_id)){
			$gump 		= new GUMP();
			$validdata 	= $gump->sanitize($data);
			if($clientinfo->typeid == 1 || $clientinfo->typeid == 4)
			{
				$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,65535'));
			}
			elseif($clientinfo->typeid == 2 || $clientinfo->typeid == 5 || $clientinfo->typeid == 6)
			{
				$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,99999999'));
			}
			elseif($clientinfo->typeid == 8)
			{
				$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,4294967295'));
			}
			else
			{
				$gump->validation_rules(array('slc_id'=> 'required|numeric|min_len,1|max_numeric,0000000099999999'));
			}
			$gump->filter_rules(array('slc_id'=> 'trim'));
			$validated_data = $gump->run($validdata);
			if($validated_data === false) 
			{
				$errors = $gump->get_readable_errors(true, '', '', $user_id);
				$errors = str_replace("field","SLC ID",$errors);
				echo json_encode(array("status" => 'error',"msg" => $errors));
				exit();
			}
			if(!is_numeric($slc_id))
			{
				echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_ID_NOT_NUMERIC']));
				exit();
			}
		}

		$sql 	=	"SELECT vShowAllSLC, clientid, hosted_server FROM slc_clients WHERE id = ".$client_id." LIMIT 1";
		$stmt	=	$db->query($sql);
		$dataa	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		//echo "<pre>";print_r($dataa);exit;
		if($dataa[0]->vShowAllSLC == 'No'){
			//$sql1 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND client_id = '".$client_id."' LIMIT 1";

			$sql1 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.client_id = '".$client_id."' AND a.address != '' AND a.user_id in (select id from slc_users where client_id = ".$client_id." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt1 		=	$db->query($sql1);
			$result1 	=	$stmt1->fetchAll(PDO::FETCH_OBJ);
			if(isset($result1[0]->tot) && $result1[0]->tot > 0){
				//$sql2 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

				$sql2 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND a.address != '' AND a.user_id in (select id from slc_users where client_id = ".$client_id." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";

				$stmt2 		=	$db->query($sql2);
				$result2 	=	$stmt2->fetchAll(PDO::FETCH_OBJ);
				if(isset($result2[0]->tot) && $result2[0]->tot == 0){
					if($clientinfo->typeid == 8){
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_UID_OR_SLCID']));
					}else{
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_MACID_OR_SLCID']));
					}
					$db = null;
					exit();
				}
			}

			//$sql3 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

			$sql3 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND a.address != '' AND a.user_id in (select id from slc_users where client_id = ".$client_id." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt3 		=	$db->query($sql3);
			$result3 	=	$stmt3->fetchAll(PDO::FETCH_OBJ);
			if(isset($result3[0]->tot) && $result3[0]->tot > 0){
				//$sql4 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";
				$sql4 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND a.address != '' AND a.user_id in (select id from slc_users where client_id = ".$client_id." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
				$stmt4 		=	$db->query($sql4);
				$result4 	=	$stmt4->fetchAll(PDO::FETCH_OBJ);
				if(isset($result4[0]->tot) && $result4[0]->tot == 0){
					if($clientinfo->typeid == 8){
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_UID_OR_SLCID']));
					}else{
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_MACID_OR_SLCID']));
					}
					$db = null;
					exit();
				}
			}

			//$sql 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";
			$sql = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND a.address != '' AND a.user_id in (select id from slc_users where client_id = ".$client_id." AND username = '".$userInfo->username."') AND sc.clientid = '".$dataa[0]->clientid."' AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt 		=	$db->query($sql);
			$result 	=	$stmt->fetchAll(PDO::FETCH_OBJ);
			if(isset($result[0]->tot) && $result[0]->tot > 0){
				echo json_encode(array("status" => 'continue', "msg" => $lvars['OVERWRTING_EXISTING_SLC']));
				$db = null;
				exit();
			}else{
				echo json_encode(array("status" => 'success'));
				$db = null;
				exit();
			}
		}else{
			//$sql1 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND client_id = '".$client_id."' LIMIT 1";

			$sql1 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.client_id = '".$client_id."' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt1 		=	$db->query($sql1);
			$result1 	=	$stmt1->fetchAll(PDO::FETCH_OBJ);
			if(isset($result1[0]->tot) && $result1[0]->tot > 0){
				//$sql2 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

				$sql2 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
				$stmt2 		=	$db->query($sql2);
				$result2 	=	$stmt2->fetchAll(PDO::FETCH_OBJ);
				if(isset($result2[0]->tot) && $result2[0]->tot == 0){
					if($clientinfo->typeid == 8){
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_UID_OR_SLCID']));
					}else{
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_MACID_OR_SLCID']));
					}
					$db = null;
					exit();
				}
			}

			//$sql3 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

			$sql3 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt3 		=	$db->query($sql3);
			$result3 	=	$stmt3->fetchAll(PDO::FETCH_OBJ);
			if(isset($result3[0]->tot) && $result3[0]->tot > 0){
				//$sql4 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

				$sql4 = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
				$stmt4 		=	$db->query($sql4);
				$result4 	=	$stmt4->fetchAll(PDO::FETCH_OBJ);
				if(isset($result4[0]->tot) && $result4[0]->tot == 0){
					if($clientinfo->typeid == 8){
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_UID_OR_SLCID']));
					}else{
						echo json_encode(array("status" => 'error', "msg" => $lvars['DUPLICATE_MACID_OR_SLCID']));
					}
					$db = null;
					exit();
				}
			}

			//$sql 		=	"SELECT count(id) as tot FROM slc_installation_data WHERE mac_address = '".$mac_address."' AND slc_id = '".$slc_id."' AND client_id = '".$client_id."' LIMIT 1";

			$sql = "SELECT count(a.id) as tot FROM slc_installation_data AS a LEFT JOIN slc_clients sc ON a.client_id = sc.id WHERE a.mac_address = '".$mac_address."' AND a.slc_id = '".$slc_id."' AND a.client_id = '".$client_id."' AND sc.clientid = '".$dataa[0]->clientid."' AND a.user_id in (select id from slc_users WHERE username = '".$userInfo->username."') AND sc.hosted_server = '".$dataa[0]->hosted_server."' LIMIT 1";
			$stmt 		=	$db->query($sql);
			$result 	=	$stmt->fetchAll(PDO::FETCH_OBJ);
			if(isset($result[0]->tot) && $result[0]->tot > 0){
				echo json_encode(array("status" => 'continue', "msg" => $lvars['OVERWRTING_EXISTING_SLC']));
				$db = null;
				exit();
			}else{
				echo json_encode(array("status" => 'success'));
				$db = null;
				exit();
			}
		}
	}else{	
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
		$db = null;
		exit();
	}
}
function getDiff($selecteddatetime,$currentdatetime)
{
	$diff 	= strtotime($selecteddatetime) - strtotime($currentdatetime);
	$m 		= ($diff/60);
	return round($m);
}
function getConnection() 
{ 
	global $cnf;
	$dbhost	= 	$cnf->dbhost;
	$dbuser	=	$cnf->dbuse;
	$dbpass	=	$cnf->dbpass;
	$dbname	=	$cnf->dbname;
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}
function dateConvertToYMD($date)
{
	$date	=	explode("-",$date);
	$Y		=	$date[2];
	$m		=	$date[0];
	$d		=	$date[1];
	return $Y.'-'.$m.'-'.$d;
}
function metersToFeetInches($meters, $echo = true)
{
	$m 			= $meters;
	$valInFeet 	= $m * 3.2808399;
	$valFeet 	= (int)$valInFeet;
	//$valFeet 	= round($valInFeet, 0, PHP_ROUND_HALF_UP);
	$valInches 	= round(($valInFeet-$valFeet)*12);
	if($valInches > 0)
	{
		$data = $valFeet.'\' '.$valInches.'"';
	}
	else
	{
		$data = $valFeet.'\'';
	}
	if($echo == true)
	{
		return $data;
	} 
	else 
	{
		return $data;
	}
}
function cmToInches($cmheight)
{
	$height = $cmheight * 0.393700787;
	$height = round(floatval($height), 2);
	return $height;
}
function inchesTocm($inch)
{
	$height = $inch * 2.54;
	$height = round(floatval($height), 2);
	return $height;
}
function feetInches($feet)
{
	$explode 	= explode("'",$feet);
	$feet 		= isset($explode[0])?$explode[0]:"";
	$inches 	= isset($explode[1])?$explode[1]:"";
	if(!empty($feet) && is_numeric($feet))
	{
	   $feet = $feet.' feet';
	   if(isset($inches) && !empty($inches)) 
	   {
			$explodeInc 	= explode('"',$inches);
			if(!is_numeric($explodeInc[0]) || empty($explodeInc[0]) || $explodeInc[0] >= 12)
			{
				return 'error';
			}
			else
			{
				$feet .= ' '.$explodeInc[0].' inch';
			}
	   }
	   return $feet; 
	}
	else
	{
		return 'error';
	}
}
function addOtherData()
{
	global $cnf;
	$db 			= 	getConnection();
	$data			= 	Slim::getInstance()->request()->post();
	$tag			=	isset($data['tag']) && !empty($data['tag']) ? trim($data['tag']):'';
	$name			=	isset($data['name']) && !empty($data['name']) ? trim($data['name']):'';
	$client_id		=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id		=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$mu				=	isset($data['measurement_units']) && !empty($data['measurement_units']) ? trim($data['measurement_units']):'None';
	$options		= 	array('PoleHeight','ArmLength','Diameter','FixturesPerPole','Wattage');
	$lvars			=	getLanguage($user_id,'None');
	//check code
	if(!empty($client_id) && !empty($tag) && !empty($name))
	{
		//sql query
		$sql 					=	"SELECT a.assets FROM slc_setting as a where a.client_id =".$client_id;
		$stmt					=	$db->query($sql);
		$result					= 	$stmt->fetchAll(PDO::FETCH_OBJ);
		$assets					= 	isset($result[0]->assets)? json_decode($result[0]->assets):array();
		$postKey 				=	explode("-k-",$tag);
		//$assetsName 			=	str_replace("_"," ",$postKey[0]);
		//$attributeName 		= 	str_replace("_"," ",$postKey[1]);
		$assetsName 			=	isset($postKey[0])?$postKey[0]:"";
		$attributeName 			= 	isset($postKey[1])?$postKey[1]:"";
		$assetsName 			=	trim($assetsName);
		$attributeName 			= 	trim($attributeName);
		$unique					=	true;
		$response				=	"";
		
		//check tag validations
		if(in_array($attributeName, $options))
  		{
			$validationArray[$attributeName] = $name;
			$gump 		= new GUMP();
			$validdata 	= $gump->sanitize($validationArray);
			$gump->validation_rules(array($attributeName=> 'required|numeric'));
			$gump->filter_rules(array($attributeName=> 'trim'));
			$validated_data = $gump->run($validdata);
			if($validated_data === false) 
			{
				$Attlabel  		= $attributeName;
				$AttributeSplit = preg_split('/(?=[A-Z])/', $attributeName , -1,  PREG_SPLIT_NO_EMPTY);
				if(count($AttributeSplit) > 0)
				{
					$Attlabel 	= "";
					foreach($AttributeSplit as $val)
					{
						$Attlabel .= $val.'';
					}
					$Attlabel 	= str_replace("_"," ",$Attlabel);
				}
				$errors = $gump->get_readable_errors(true, '', '', $user_id);
				$errors = str_replace("field",$Attlabel,$errors);
				echo json_encode(array("status" => 'error',"msg" => $errors));
				exit();
			}
		}
		$clientinfo 	= 	getClientInfo($client_id);
		//echo "<pre>";print_r($clientinfo);exit;

		if($clientinfo->vLGVersion == 'LG5'){
			if(isset($assets->assets) && count($assets->assets) > 0)
			{
				foreach($assets->assets as $key=> $val)
				{
					if(isset($val->assetName) && trim($val->assetName) == $assetsName)
					{
						foreach($val->attributes as $skey=> $sval)
						{
							if(trim($sval->attributeName) == $attributeName)
							{
								if($attributeName == 'Pole Height' || $attributeName == 'Arm Length' || $attributeName == 'Diameter')
								{
									if($mu == 'English')
									{
										if($attributeName == 'Pole Height' || $attributeName == 'Arm Length')
										{
											$name = $name.' feet';
										}
										else
										{
											$name = $name.' inch';
										}
									}
									else
									{
										if($attributeName == 'Pole Height' || $attributeName == 'Arm Length')
										{
											if($name <= 1)
											{
												$name = $name.' meter';
											}
											else
											{
												$name = $name.' meters';
											}
										}
										else
										{
											$name = $name.' cm';
										}	
									}
								}
								if(isset($sval->values) && count($sval->values) > 0)
								{
									/*foreach($sval->values as $vval)
									{
										if($vval == $name)
										{
											$unique	= false;
											break;
										}
									}*/
									//check unique
									//if($unique)
									//{
										$cnt = count($sval->values);
										$assets->assets[$key]->attributes[$skey]->values[$cnt] = $name;
										//update
										$updatedAssets	=	json_encode($assets);
										$sql 			=	"UPDATE slc_setting SET assets = '".addslashes($updatedAssets)."' WHERE client_id = ".$client_id;
										$stmts 			=	$db->query($sql);
										//get updated assets
										$settings 		=	getSettingList($client_id,$assetsName,$attributeName,$mu);
										//response
										$response = array("status" => 'success','msg'=>'Added Successfully','data'=>$settings,'label'=>$name);
									//}
									/*else
									{
										$Attlabel  		= $attributeName;
										$AttributeSplit = preg_split('/(?=[A-Z])/', $attributeName , -1,  PREG_SPLIT_NO_EMPTY);
										if(count($AttributeSplit) > 0)
										{
											$Attlabel 	= "";
											foreach($AttributeSplit as $val)
											{
												$Attlabel .= $val.' ';
											}
											$Attlabel 	= str_replace("_"," ",$Attlabel);	
										}
										$errors = $lvars['UNIQUE_VALUE'];
										$errors = str_replace("Value",$Attlabel,$errors);
										//response
										$response = array("status" => 'error','msg'=>$errors);
									}*/
								}
								else
								{
									$assets->assets[$key]->attributes[$skey]->values = array($name);
									//update
									$updatedAssets	=	json_encode($assets);
									$sql 			=	"UPDATE slc_setting SET assets = '".addslashes($updatedAssets)."' WHERE client_id = ".$client_id;
									$stmts 			=	$db->query($sql);
									//get updated assets
									$settings 		=	getSettingList($client_id,$assetsName,$attributeName,$mu);
									//response
									$response = array("status" => 'success','msg'=>$lvars['OTHER_DATA_ADD_SUCCESSFULLY'],'data'=>$settings,'label'=>$name);
								}
							}
						}
					}
				}
				//response
				if(empty($response))
				{
					echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_ATTR']));
				}
				else
				{
					echo json_encode($response);
				}
			}
			else
			{
				//response
				echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_ASSETS']));
			}
		}else{
			if(isset($assets->Assets) && count($assets->Assets) > 0)
			{
				foreach($assets->Assets as $key=> $val)
				{
					if(isset($val->AssetName) && trim($val->AssetName) == $assetsName)
					{
						foreach($val->Attributes as $skey=> $sval)
						{
							if(trim($sval->AttributeName) == $attributeName)
							{
								if($attributeName == 'PoleHeight' || $attributeName == 'ArmLength' || $attributeName == 'Diameter')
								{
									if($mu == 'English')
									{
										if($attributeName == 'PoleHeight' || $attributeName == 'ArmLength')
										{
											$name = $name.' feet';
										}
										else
										{
											$name = $name.' inch';
										}
									}
									else
									{
										if($attributeName == 'PoleHeight' || $attributeName == 'ArmLength')
										{
											if($name <= 1)
											{
												$name = $name.' meter';
											}
											else
											{
												$name = $name.' meters';
											}
										}
										else
										{
											$name = $name.' cm';
										}	
									}
								}
								if(isset($sval->Values) && count($sval->Values) > 0)
								{
									/*foreach($sval->Values as $vval)
									{
										if($vval == $name)
										{
											$unique	= false;
											break;
										}
									}*/
									//check unique
									//if($unique)
									//{
										$cnt = count($sval->Values);
										$assets->Assets[$key]->Attributes[$skey]->Values[$cnt] = $name;
										//update
										$updatedAssets	=	json_encode($assets);
										$sql 			=	"UPDATE slc_setting SET assets = '".addslashes($updatedAssets)."' WHERE client_id = ".$client_id;
										$stmts 			=	$db->query($sql);
										//get updated assets
										$settings 		=	getSettingList($client_id,$assetsName,$attributeName,$mu);
										//response
										$response = array("status" => 'success','msg'=>'Added Successfully','data'=>$settings,'label'=>$name);
									/*}
									else
									{
										$Attlabel  		= $attributeName;
										$AttributeSplit = preg_split('/(?=[A-Z])/', $attributeName , -1,  PREG_SPLIT_NO_EMPTY);
										if(count($AttributeSplit) > 0)
										{
											$Attlabel 	= "";
											foreach($AttributeSplit as $val)
											{
												$Attlabel .= $val.' ';
											}
											$Attlabel 	= str_replace("_"," ",$Attlabel);	
										}
										$errors = $lvars['UNIQUE_VALUE'];
										$errors = str_replace("Value",$Attlabel,$errors);
										//response
										$response = array("status" => 'error','msg'=>$errors);
									}*/
								}
								else
								{
									$assets->Assets[$key]->Attributes[$skey]->Values = array($name);
									//update
									$updatedAssets	=	json_encode($assets);
									$sql 			=	"UPDATE slc_setting SET assets = '".addslashes($updatedAssets)."' WHERE client_id = ".$client_id;
									$stmts 			=	$db->query($sql);
									//get updated assets
									$settings 		=	getSettingList($client_id,$assetsName,$attributeName,$mu);
									//response
									$response = array("status" => 'success','msg'=>$lvars['OTHER_DATA_ADD_SUCCESSFULLY'],'data'=>$settings,'label'=>$name);
								}
							}
						}
					}
				}
				//response
				if(empty($response))
				{
					echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_ATTR']));
				}
				else
				{
					echo json_encode($response);
				}
			}
			else
			{
				//response
				echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_ASSETS']));
			}
		}
	}
	else
	{
		//response
		echo json_encode(array("status" => 'error','msg'=>$lvars['NO_REQUSET_DATA']));
	}
    exit();
}
function getLastSLC($client_id,$mu)
{
	global $cnf;
	$db 		= 	getConnection();
	$sql 		=	"SELECT a.client_id,a.user_id,a.slc_id,a.measurement_units,a.pole_id,a.assets,a.pole_image,a.date_of_installation FROM slc_installation_data AS a where a.client_id = ".$client_id." order by a.ID DESC LIMIT 1";
	$stmt		=	$db->query($sql);
	$data		= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	$Assets		= 	array();
	$lvars		=	getLanguage($data[0]->user_id,'None');
	$date_of_installation = "";
	$Assets = getClientAssets($data[0]->client_id,$data[0]->measurement_units,$data[0]->assets);
	$date_of_installation 	=	date('m-d-Y',strtotime($data[0]->date_of_installation));
	$measurement_units 		=	isset($data[0]->measurement_units)?$data[0]->measurement_units:"";
	if($measurement_units == "" || $measurement_units == "None")
	{
		$measurement_units = $mu;
	}
	$msg 					=	str_replace("#VALUE#",$measurement_units,$lvars['NOTE_CHANGE_MU']);
	$slccount 				=	getSLCCount($client_id);
	return array("status" => 'success',"msg" => $msg, 'slccount'=>$slccount, 'data'=>$Assets,'measurement_units'=>$measurement_units,'date_of_installation'=>$date_of_installation);
}
function getSetting($client_id,$type="json")
{
	//echo $type;exit;
	$db = getConnection();
	$sql = "SELECT * FROM slc_setting where client_id =".$client_id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	$options = array();
	$options['client_slc_list_view'] = isset($result[0]->client_slc_list_view)?$result[0]->client_slc_list_view:"No";
	$options['client_slc_edit_view'] = isset($result[0]->client_slc_edit_view)?$result[0]->client_slc_edit_view:"No";
	$options['client_slc_pole_image_view'] = isset($result[0]->client_slc_pole_image_view)?$result[0]->client_slc_pole_image_view:"No";
	$options['client_slc_pole_assets_view'] = isset($result[0]->client_slc_pole_assets_view)?$result[0]->client_slc_pole_assets_view:"No";
	$options['client_slc_pole_id'] = isset($result[0]->client_slc_pole_id)?$result[0]->client_slc_pole_id:"No";
	//echo "<pre>";print_r($options);exit;
	if($type == "json")
	{
		echo json_encode(array("status" => 'success','data'=> isset($options)?$options:array()));
		exit();
	}
	else
	{
		//echo "<pre>";print_r($options);exit;
		return isset($options)?$options:array();
	}
}
function getAssets($client_id,$mu='None',$copy='No')
{
	if(isset($copy) && $copy == "Yes")
	{
		$slcAssets = getLastSLC($client_id,$mu);
		echo json_encode($slcAssets);
		exit();
	}
	else
	{
		$db = getConnection();

		$clientinfo 	= 	getClientInfo($client_id);
		//echo "<pre>";print_r($clientinfo);exit;

		$sql = "SELECT * FROM slc_setting where client_id =".$client_id;
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;
		$options = array();
		if(isset($result[0]->assets))
		{
			$assets = json_decode($result[0]->assets);
			//echo $clientinfo->vLGVersion;exit;
			if($clientinfo->vLGVersion == 'LG5'){
				if(isset($assets->assets) && count($assets->assets) > 0)
				{
					$cnt = 0;
					foreach($assets->assets as $val)
					{
						$AssetName = $val->assetName;
						$AssetsKey = trim($AssetName);
						//$AssetsKey = str_replace(" ","_",$AssetsKey);
						if(isset($val->attributes) && count($val->attributes) > 0)
						{
							foreach($val->attributes as $sval)
							{
								$AttrKey = trim($sval->attributeName);
								if($AttrKey != "PoleID")
								{
									//$AttrKey = str_replace(" ","_",$AttrKey);
									$AttrKey = $AssetsKey.'-k-'.$AttrKey;
									$valOptions = array();
									if(isset($sval->values) && count($sval->values) > 0 && (trim($sval->attributeName) == 'Pole Height' || trim($sval->attributeName) == 'Arm Length' || trim($sval->attributeName) == 'Diameter'))
									{
										if(trim($sval->attributeName) == 'Pole Height')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->attributeName) == 'Arm Length')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->attributeName) == 'Diameter')
										{
											foreach($sval->values as $vval)
											{
												$checkKey = $mu == 'English'?'inch':"cm";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
									}
											
									if(empty($valOptions))
									{
										$valOptions = $sval->values;
									}
									//get name
									$Attlabel  		= $sval->attributeName;
									$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->attributeName , -1,  PREG_SPLIT_NO_EMPTY);
									$Note 			= 'Select '.$sval->attributeName;
									$BtnText 		= 'Select '.$sval->attributeName; 
									if(count($AttributeSplit) > 0)
									{
										$Attlabel 	= "";
										$Note 		= 'Select ';
										$BtnText 	= 'Select ';
										foreach($AttributeSplit as $val)
										{
											$Note .= $val.' ';
											$BtnText .= $val.' ';
											$Attlabel .= $val.'';
										}
										$Note 		= str_replace("_"," ",$Note);
										$BtnText 	= str_replace("_"," ",$BtnText);
										$Attlabel 	= str_replace("_","",$Attlabel);	
									}
									//sort array
									if(isset($valOptions) && count($valOptions) > 0)
									{
										$valOptions = sortArray($valOptions);
									}

									if(isset($sval->isRequire)) {
										$isRequire = $sval->isRequire;
									}else{
										$isRequire = '';
									}
									if(isset($sval->ispicklist)){
										$ispicklist = $sval->ispicklist;
									}else{
										$ispicklist = '';
									}
									if(isset($sval->type)){
										$type = $sval->type;
									}else{
										$type = '';
									}
									$isRequire  = $isRequire;
									$ispicklist = $ispicklist;
									$type  		= $type;
									//$ispicklist = $sval->ispicklist;
									//$type  		= $sval->type;
									//return array
									$options[$cnt] = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>'','isRequire'=>$isRequire,'ispicklist'=>$ispicklist,'type'=>$type);
									$cnt++;
								}
							}
						}
					}
				}
			}else{
				if(isset($assets->Assets) && count($assets->Assets) > 0)
				{
					$cnt = 0;
					foreach($assets->Assets as $val)
					{
						$AssetName = $val->AssetName;
						$AssetsKey = trim($AssetName);
						//$AssetsKey = str_replace(" ","_",$AssetsKey);
						if(isset($val->Attributes) && count($val->Attributes) > 0)
						{
							foreach($val->Attributes as $sval)
							{
								$AttrKey = trim($sval->AttributeName);
								if($AttrKey != "PoleID")
								{
									//$AttrKey = str_replace(" ","_",$AttrKey);
									$AttrKey = $AssetsKey.'-k-'.$AttrKey;
									$valOptions = array();
									if(isset($sval->Values) && count($sval->Values) > 0 && (trim($sval->AttributeName) == 'PoleHeight' || trim($sval->AttributeName) == 'ArmLength' || trim($sval->AttributeName) == 'Diameter'))
									{
										if(trim($sval->AttributeName) == 'PoleHeight')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->AttributeName) == 'ArmLength')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'feet':"meter";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
										elseif(trim($sval->AttributeName) == 'Diameter')
										{
											foreach($sval->Values as $vval)
											{
												$checkKey = $mu == 'English'?'inch':"cm";
												if (strpos($vval, $checkKey) !== false) 
												{
													$valOptions[] = $vval;
												}
											}
										}
									}
											
									if(empty($valOptions))
									{
										$valOptions = $sval->Values;
									}
									//get name
									$Attlabel  		= $sval->AttributeName;
									$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->AttributeName , -1,  PREG_SPLIT_NO_EMPTY);
									$Note 			= 'Select '.$sval->AttributeName;
									$BtnText 		= 'Select '.$sval->AttributeName; 
									if(count($AttributeSplit) > 0)
									{
										$Attlabel 	= "";
										$Note 		= 'Select ';
										$BtnText 	= 'Select ';
										foreach($AttributeSplit as $val)
										{
											$Note .= $val.' ';
											$BtnText .= $val.' ';
											$Attlabel .= $val.'';
										}
										$Note 		= str_replace("_"," ",$Note);
										$BtnText 	= str_replace("_"," ",$BtnText);
										$Attlabel 	= str_replace("_","",$Attlabel);	
									}
									//sort array
									if(isset($valOptions) && count($valOptions) > 0)
									{
										$valOptions = sortArray($valOptions);
									}
									//return array
									$options[$cnt] = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>'');
									$cnt++;
								}
							}
						}
					}
				}
			}
		}
		$slccount = getSLCCount($client_id);
		echo json_encode(array("status" => 'success','data'=>$options,'slccount'=>$slccount));
	 	exit();
	}
}
function sortArray($valOptions)
{
	sort($valOptions);
	return $valOptions;
}
function getcheckKeyExist($AssetName,$assets)
{
	if(isset($assets) && count($assets) > 0)
	{
		foreach($assets as $val)
		{
			if(isset($val['AssetName']) && $val['AssetName'] == $AssetName)
			{
				return false;
				break;	
			}
		}
		return true;
	}
	else
	{
		return true;
	}
}
function getConvertAssets($data)
{
	$data = json_decode($data);
	//echo "<pre>";print_r($data);exit;
	$options = array();
	if(isset($data) && count($data) > 0)
	{
		$cnt = 0;
		//find assets array
		foreach($data as $key=>$val)
		{
			//echo "<pre>";print_r($data);
			//echo $key;
			//echo "==".$val;
			//if(isset($key) && !empty($key) && isset($val) && !empty($val))
			if(isset($key) && !empty($key))
			{
				//echo "string";exit;
				$postKey 		= explode("-k-",$key);
				$assetsName 	= $postKey[0];	
				//$assetsName 	= str_replace("_"," ",$assetsName);
				$assetsArray 	= isset($options['Assets'])?$options['Assets']:array();
				$keyExist 		= getcheckKeyExist($assetsName,$assetsArray);
				if($keyExist) 
				{
					$options['Assets'][$cnt]['AssetName'] = $assetsName;
					$cnt++;
				}
			}
		}
		//echo "<pre>";print_r($options);exit;
		$options['AssetCount'] = isset($options['Assets']) ? count($options['Assets']) : 0;
		//find attribute array
		$j=0;
		if(isset($options['Assets']))
		{
			foreach($options['Assets'] as $okey=>$oval)
			{
				$i=0;
				$suboptions = array();
				foreach($data as $key=>$val)
				{
					$postKey 		= explode("-k-",$key);
					$assetsName 	= isset($postKey[0])?$postKey[0]:"";
					//$assetsName 	= str_replace("_"," ",$assetsName);
					if($oval['AssetName'] == $assetsName)
					{
						//$attributeName = str_replace("_"," ",$postKey[1]);
						$attributeName = isset($postKey[1])?$postKey[1]:"";
						$suboptions[$i]['AttributeName'] = $attributeName;
						$suboptions[$i]['Values'] = isset($val)?$val:"";
						$options['Assets'][$j]['Attributes'] = $suboptions;
						$i++;
					}
				}
				$j++;
			}
		}
	}
	//return array
	return json_encode($options);
}
function getClientAssets($id,$mu,$selectedAssets)
{
	$db = getConnection();

	$clientinfo 	= 	getClientInfo($id);
	//echo "<pre>";print_r($clientinfo);exit;
	
	$sql = "SELECT * FROM slc_setting where client_id =".$id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	$options = array();
	if(isset($result[0]->assets))
	{
		//echo "<pre>";print_r($result[0]->assets);exit;
		$assets 		=	json_decode($result[0]->assets);
		$selectedAssets	=	json_decode($selectedAssets);

		if($clientinfo->vLGVersion == 'LG5'){
			if(isset($assets->assets) && count($assets->assets) > 0)
			{
				$cnt = 0;
				foreach($assets->assets as $val)
				{
					$AssetName = trim($val->assetName);
					$AssetsKey = trim($AssetName);
					//$AssetsKey = str_replace(" ","_",$AssetsKey);
					if(isset($val->attributes) && count($val->attributes) > 0)
					{
						foreach($val->attributes as $atkey=> $sval)
						{
							$AttrKey 	= trim($sval->attributeName);
							if($AttrKey != "PoleID")
							{
								//$AttrKey 	= str_replace(" ","_",$AttrKey);
								$AttrKey 	= $AssetsKey.'-k-'.$AttrKey;
								$selected 	= "None";
								$findselected = findAssetsSelected($AssetName,$sval->attributeName,$selectedAssets);
								if($findselected != "")
								{

									$selected = $findselected;
								}
								$valOptions = array();
								if(isset($sval->values) && count($sval->values) > 0 && (trim($sval->attributeName) == 'Pole Height' || trim($sval->attributeName) == 'Arm Length' || trim($sval->attributeName) == 'Diameter'))
								{
									if(trim($sval->attributeName) == 'Pole Height')
									{
										foreach($sval->values as $vval)
										{
											$checkKey = $mu == 'English'?'feet':"meter";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
									elseif(trim($sval->attributeName) == 'Arm Length')
									{
										foreach($sval->values as $vval)
										{
											$checkKey = $mu == 'English'?'feet':"meter";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
									elseif(trim($sval->attributeName) == 'Diameter')
									{
										foreach($sval->values as $vval)
										{
											$checkKey = $mu == 'English'?'inch':"cm";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
								}
								if(empty($valOptions))
								{
									$valOptions = $sval->values;
								}
								//get name
								$Attlabel  		= $sval->attributeName;
								$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->attributeName , -1,  PREG_SPLIT_NO_EMPTY);
								$Note 			= 'Select '.$sval->attributeName;
								$BtnText 		= 'Select '.$sval->attributeName;
								if(count($AttributeSplit) > 0)
								{
									$Attlabel 	= "";
									$Note 		= 'Select ';
									$BtnText 	= 'Select ';
									foreach($AttributeSplit as $val)
									{
										$Note .= $val.' ';
										$BtnText .= $val.' ';
										$Attlabel .= $val.'';
									}
									$Note 		= str_replace("_"," ",$Note);
									$BtnText 	= str_replace("_"," ",$BtnText);
									$Attlabel 	= str_replace("_","",$Attlabel);	
								}
								//sort array
								if(isset($valOptions) && count($valOptions) > 0)
								{
									$valOptions = sortArray($valOptions);
								}

								if(isset($sval->isRequire)) {
									$isRequire = $sval->isRequire;
								}else{
									$isRequire = '';
								}
								if(isset($sval->ispicklist)){
									$ispicklist = $sval->ispicklist;
								}else{
									$ispicklist = '';
								}
								if(isset($sval->type)){
									$type = $sval->type;
								}else{
									$type = '';
								}
								$isRequire  = $isRequire;
								$ispicklist = $ispicklist;
								$type  		= $type;

								//return
								$options[$cnt] = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>$selected,'isRequire'=>$isRequire,'ispicklist'=>$ispicklist,'type'=>$type);
								$cnt++;
							}
						}
					}
				}
			}
		}else{
			if(isset($assets->Assets) && count($assets->Assets) > 0)
			{
				$cnt = 0;
				foreach($assets->Assets as $val)
				{
					$AssetName = trim($val->AssetName);
					$AssetsKey = trim($AssetName);
					//$AssetsKey = str_replace(" ","_",$AssetsKey);
					if(isset($val->Attributes) && count($val->Attributes) > 0)
					{
						foreach($val->Attributes as $atkey=> $sval)
						{
							$AttrKey 	= trim($sval->AttributeName);
							if($AttrKey != "PoleID")
							{
								//$AttrKey 	= str_replace(" ","_",$AttrKey);
								$AttrKey 	= $AssetsKey.'-k-'.$AttrKey;
								$selected 	= "None";
								$findselected = findAssetsSelected($AssetName,$sval->AttributeName,$selectedAssets);
								if($findselected != "")
								{

									$selected = $findselected;
								}
								$valOptions = array();
								if(isset($sval->Values) && count($sval->Values) > 0 && (trim($sval->AttributeName) == 'PoleHeight' || trim($sval->AttributeName) == 'ArmLength' || trim($sval->AttributeName) == 'Diameter'))
								{
									if(trim($sval->AttributeName) == 'PoleHeight')
									{
										foreach($sval->Values as $vval)
										{
											$checkKey = $mu == 'English'?'feet':"meter";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
									elseif(trim($sval->AttributeName) == 'ArmLength')
									{
										foreach($sval->Values as $vval)
										{
											$checkKey = $mu == 'English'?'feet':"meter";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
									elseif(trim($sval->AttributeName) == 'Diameter')
									{
										foreach($sval->Values as $vval)
										{
											$checkKey = $mu == 'English'?'inch':"cm";
											if (strpos($vval, $checkKey) !== false) 
											{
												$valOptions[] = $vval;
											}
										}
									}
								}
								if(empty($valOptions))
								{
									$valOptions = $sval->Values;
								}
								//get name
								$Attlabel  		= $sval->AttributeName;
								$AttributeSplit = preg_split('/(?=[A-Z])/', $sval->AttributeName , -1,  PREG_SPLIT_NO_EMPTY);
								$Note 			= 'Select '.$sval->AttributeName;
								$BtnText 		= 'Select '.$sval->AttributeName; 
								if(count($AttributeSplit) > 0)
								{
									$Attlabel 	= "";
									$Note 		= 'Select ';
									$BtnText 	= 'Select ';
									foreach($AttributeSplit as $val)
									{
										$Note .= $val.' ';
										$BtnText .= $val.' ';
										$Attlabel .= $val.'';
									}
									$Note 		= str_replace("_"," ",$Note);
									$BtnText 	= str_replace("_"," ",$BtnText);
									$Attlabel 	= str_replace("_"," ",$Attlabel);
								}
								//sort array
								if(isset($valOptions) && count($valOptions) > 0)
								{
									$valOptions = sortArray($valOptions);
								}
								//return
								$options[$cnt] = array('AttrKey'=>$AttrKey,'AssetName'=>$AssetName,'Note'=>trim($Note),'BtnText'=>trim($BtnText),'AttributeName'=>trim($Attlabel),'Values'=>$valOptions,'Selected'=>$selected);
								$cnt++;
							}
						}
					}
				}
			}
		}
	}
	//echo "<pre>";print_r($options);exit;
	return $options;
}
function findAssetsSelected($Asset,$Attribute,$SelectedAssets)
{
	if(isset($SelectedAssets->Assets) && count($SelectedAssets->Assets) > 0)
	{
		foreach($SelectedAssets->Assets as $val)
		{
			if(trim($Asset) == $val->AssetName)
			{
				foreach($val->Attributes as $sval)
				{
					if(trim($Attribute) == $sval->AttributeName)
					{
						return $sval->Values;
						break;
					}
				}
			}
		}
	}
	return false;
}
function checkLogin($username,$password,$ClientID,$ApiServer,$source,$lvars)
{
	set_time_limit(0);
	global $cnf;
	$ipaddress	= $_SERVER['REMOTE_ADDR'];
	$ClientID 	= encryption($ClientID);
	$starttime  = date('H:i:s');
	$headers 	= array('Content-Type:application/json');
	$ch 		= curl_init();
	curl_setopt($ch, CURLOPT_URL,$ApiServer.'get-token');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: 0','username:'.$username, 'password:'.$password, 'clientid:'.$ClientID, 'IsSecured:1'));
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers)
	{
		$len = strlen($header);
		$header = explode(':', $header, 2);
		if (count($header) >= 2)
			$headers[strtolower(trim($header[0]))] = trim($header[1]);
		return $len;
	});
	$send_response = curl_exec($ch);
	$info = curl_getinfo($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//check response
	if ($send_response === FALSE) 
	{
		return array("Message"=>$lvars['CONNECTION_ERROR']);
		//return false;
	}
	curl_close($ch);
	//return response
	if($httpcode == 200)
	{
		//generate log
		$filename = ROOT_LOGSPATH.'LG-LOGIN-SUCCESS-'.$source.'-'.$ipaddress.'-'.time().'.txt';
		$logfile = fopen($filename,"wb");
		fwrite($logfile,json_encode(array('starttime'=>$starttime,'endtime'=>date('H:i:s'),'response'=>$send_response,'httpcode'=>$httpcode,'ipaddress' =>$ipaddress)));
		fclose($logfile);
		return array("Message"=>$send_response,"Token"=>$headers['token']);
	}
	else
	{
		//generate log
		$filename = ROOT_LOGSPATH.'LG-LOGIN-FAIL-'.$source.'-'.$ipaddress.'-'.time().'.txt';
		$logfile = fopen($filename,"wb");
		fwrite($logfile,json_encode(array('starttime'=>$starttime,'endtime'=>date('H:i:s'),'response'=>$send_response,'httpcode'=>$httpcode,'ipaddress' =>$ipaddress)));
		fclose($logfile);
		return array("Message"=>$send_response);
	}
}
function checkLG5Login($username,$password,$ClientID,$ApiServer,$lvars,$Version,$Source)
{
	$Origin 	= 'EZInstall';
	//echo $ApiServer;exit;
	set_time_limit(0);
	global $cnf;
	$ipaddress	= $_SERVER['REMOTE_ADDR'];
	//$ClientID 	= encryption($ClientID);
	$starttime  = date('H:i:s');
	$headers 	= array('Content-Type:application/json');
	$ch 		= curl_init();
	curl_setopt($ch, CURLOPT_URL,$ApiServer.'get-token');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	//curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: 0','username:'.$username, 'password:'.$password, 'clientid:'.$ClientID));
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: 0','username:'.$username, 'password:'.$password, 'grant_type:password', 'clientid:'.$ClientID, 'IsSecured:1', 'Origin:'.$Origin, 'Version:'.$Version, 'Source:'.$Source));
	curl_setopt($ch, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$headers)
	{
		$len = strlen($header);
		$header = explode(':', $header, 2);
		if (count($header) >= 2)
			$headers[strtolower(trim($header[0]))] = trim($header[1]);
		return $len;
	});
	$send_response = curl_exec($ch);
	//echo "<pre>";print_r($send_response);exit;
	$info = curl_getinfo($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//check response
	if ($send_response === FALSE) 
	{
		return array("send_response"=>"", "Message"=>$lvars['CONNECTION_ERROR']);
		//return false;
	}
	curl_close($ch);
	//return response
	if($httpcode == 200)
	{
		return array("send_response"=>$send_response);
	}
	else
	{
		return array("send_response"=>$send_response, "response_code"=>$httpcode);
	}
}
function updateClientAssets($ClientID,$ApiServer,$token,$LGVersion)
{
	//echo $ApiServer.'get-master-assets';exit;
	set_time_limit(0);
	$headers = array('Content-Type:application/json');
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$ApiServer.'get-master-assets');
	curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
	if($LGVersion == 'LG5'){
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: 0','Authorization: bearer '.$token, 'IsSecured:1'));
	}else{
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Length: 0','token:'.$token, 'IsSecured:1'));
	}
	$send_response = curl_exec($ch);
	$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	//echo $token;exit;
	//echo $httpcode;exit;
	//echo "<pre>";print_r($send_response);exit;
	curl_close($ch);
	if(isset($httpcode) && $httpcode == 200)
	{
		$db = getConnection();
		$sql = "SELECT * FROM slc_setting where client_id =".$ClientID;
		$stmt = $db->query($sql);
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		if(count($result) > 0)
		{
			//assets
			$sql = "UPDATE slc_setting SET assets = '".addslashes($send_response)."' WHERE client_id = ".$ClientID;
			$stmts = $db->query($sql);
		}
		else
		{
			$sql = "INSERT INTO slc_setting(client_id,assets)VALUES(".$ClientID.",'".addslashes($send_response)."')";
			$stmt = $db->query($sql);
		}
		//db null
		$db = null;
	}
	return true;
}
function encryption($key)
{
	set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclib');
	include('phpseclib/Crypt/RSA.php');
	$rsa = new Crypt_RSA();
	$rsa->loadKey('<RSAKeyValue><Exponent>AQAB</Exponent><Modulus>wnaPogk6MFd80EWeg/nPIDKgb7ljfaSPGOUUkv8J78kUMpLOOsuQ0hPepOWRD4ALzekfCAKzYX15jvGvapu1R9yax0huJ20PAEmrhKjPC9AjNhiUDXPNWOvDQsybBqq3GgC2DrySuXI3vfULg+Gvd4IGcoeZxJqpULPoX6jCMpZbaRn7GavZ9LXySTE1kYjJk8SesIXPdhIXHhNPmNZkRrnY5OspvgtJ6ORfpKlGV42VrdRyPA9w4D+pvLd79S6XE6gV3FbXXVtcbHDK83/WWBpNbaLyzcY48cnf44YuGilzpOgRL1B851iT19eGt2ZC+JT4ypDuKvpnVM/UCRlA9w==</Modulus></RSAKeyValue>');
	$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
	$encrypt = $rsa->encrypt($key);
	return base64_encode($encrypt);
}
function getAssetsInfoArrayConvert($data)
{
	$assets = json_decode($data);
	$options = array();
	if(isset($assets->Assets) && count($assets->Assets) > 0)
	{
		$cnt = 0;
		foreach($assets->Assets as $val)
		{
			$options[$cnt]['AssetName'] = $val->AssetName;
			$valOptions = array();
			if(isset($val->Attributes) && count($val->Attributes) > 0)
			{
				$i = 0;
				foreach($val->Attributes as $sval)
				{
					$valOptions[$i]['AttributeName'] = trim($sval->AttributeName);
					$values	= trim($sval->Values);
					$valOptions[$i]['Value'] = !empty($values) && $values != 'None' ? $values : "";
					$i++;
				}
			}
			$options[$cnt]['Attributes'] = $valOptions;
			$cnt++;
		}
	}
	return $options;
}
function getInstalledSLCData()
{
	set_time_limit(0);
	global $cnf;
	$db 		= 	getConnection();
	$data		= 	Slim::getInstance()->request()->post();
	$ipaddress	= $_SERVER['REMOTE_ADDR'];
	$lvars		=	getLanguage('None','None');
	//generate log
	$filename = ROOT_LOGSPATH.'Get-Installed-SLC-Data-'.$ipaddress.'-'.time().'.txt';
	$logfile = fopen($filename,"wb");
	fwrite($logfile,json_encode(array('data'=>$data,'ipaddress' =>$ipaddress)));
	fclose($logfile);
	//post		
	$username		=	isset($data['username'])?$data['username']:"";
	$password		=	isset($data['password'])?$data['password']:"";
	if(!empty($username) && !empty($password))
	{
		$login = checkClientLogin($data);
		if($login['status'] == 'success')
		{
			$clientinfo 	= 	isset($login['data'])?$login['data']:array();
			if(isset($clientinfo) && !empty($clientinfo))
			{
				$client_id		=	$clientinfo->id;
				$sql 	=	"SELECT 
									a.mac_address as MACAddress,a.slc_id as SlcID,a.address as Address,a.lat as Latitude,a.lng as Longitude,
									a.pole_id as PoleID,a.pole_image as PoleImage,a.date_of_installation as DateOfInstalled,a.assets as Assets
							 FROM 
									slc_installation_data AS a 
							where 
									a.client_id = ".$client_id." 
							order by 
									a.ID DESC";
				$stmt	=	$db->query($sql);
				$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
				if(count($data) > 0)
				{
					foreach($data as $key=>$val)
					{
						if(isset($val->PoleImage) && !empty($val->PoleImage))
						{
							$data[$key]->PoleImage = IMG_URL.$val->PoleImage;
						}
					}
					echo json_encode(array("status" => 'success','data'=>$data)); 
				}
				else
				{
					echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_NOT_FOUND'])); 
				}
			}
			else
			{
				echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
			}
		}
		else
		{
			echo json_encode(array("status" => 'error','msg'=>$login['msg']));
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_USERINFORMATION']));
	}
	exit();
}
function checkClientLogin($data)
{
	global $cnf;
	$db =  getConnection();
	$lvars		=	getLanguage('None','None');
	$username	=	isset($data['username'])?trim($data['username']):"";
	$password	=	isset($data['password'])?trim($data['password']):"";
    $sql = "SELECT * FROM slc_clients where username = '".addslashes($username)."' || email = '".addslashes($username)."'";
	$stmt	=	$db->query($sql);
	$result	= 	$stmt->fetchAll(PDO::FETCH_OBJ); 
  	if(count($result) > 0)
	{
		$dbpassword = $result[0]->password;
		$password 	= $data['password'];
		if(password_verify($password,$dbpassword))
		{
			if($result[0]->status == 1)
			{
				return array("status" => 'success','data'=>$result[0]);
			}
			else
			{
				return array("status" => 'error','msg'=>$lvars['ACCOUNT_BLOCK']);
			}
		}
		else
		{
			return array("status" => 'error','msg'=>$lvars['INVALID_PASSWORD']);
		}
	}
	else
	{
		return array("status" => 'error','msg'=>$lvars['INVALID_USERNAME']);
	}
}
function getAddress()
{
	global $cnf;
	$data	= 	Slim::getInstance()->request()->post();
	$lat	=	isset($data['lat']) && !empty($data['lat']) ? trim($data['lat']):'';
	$lng	=	isset($data['lng']) && !empty($data['lng']) ? trim($data['lng']):'';
	$cid	=	isset($data['client_id']) && !empty($data['client_id']) ? trim($data['client_id']):0;
	$uid	=	isset($data['user_id']) && !empty($data['user_id']) ? trim($data['user_id']):0;
	$lvars	=	getLanguage($uid,'None');
	$slccount = "No";
	if(!empty($lat) && !empty($lng))
	{
		$slccount 	= 	getSLCCount($cid);
		$setting	=	getSetting($cid,'detail');
		//Send request and receive json data by address
		$geocodeFromLatLong = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$lng.'&result_type=street_address&sensor=false&key='.GOOGLE_KEY); 
		$output = json_decode($geocodeFromLatLong);
		if(!empty($output))
		{
			if(isset($output->status) && $output->status == "OK")
			{
				$shortAddress = $output->results[0]->address_components[0]->long_name.', '.$output->results[0]->address_components[1]->long_name;
				echo json_encode(array("status" => 'success','slccount'=>$slccount,'shortaddress'=>$shortAddress,'address'=>isset($output->results[0]->formatted_address)?$output->results[0]->formatted_address:"",'setting'=>$setting));
			}
			else
			{
				echo json_encode(array("status" => 'error','slccount'=>$slccount,'msg'=>$lvars['ADDRESS_NOT_FOUND'],'setting'=>$setting));
			}
		}
		else
		{
			echo json_encode(array("status" => 'error','slccount'=>$slccount,'msg'=>$lvars['ADDRESS_NOT_FOUND'],'setting'=>$setting));
		}
	}
	else
	{
        echo json_encode(array("status" => 'error','slccount'=>$slccount,'msg'=>$lvars['NO_REQUSET_DATA']));  
    }
	exit();
}
function copyPoleImg($slc_id,$image)
{
	$oldimagepath 	=	ROOT_IMG_PATH . $image;
	$basename 		= 	basename($oldimagepath);
	$extention 		=	pathinfo($basename, PATHINFO_EXTENSION);
	$now_fileName 	=	'SLC-'.$slc_id.'-Pole-'.time().".".$extention;
	$newimagepath 	=	ROOT_IMG_PATH . $now_fileName;
	if (copy($oldimagepath , $newimagepath)) 
	{
		return $now_fileName;
	}
	else
	{
		return '';
	}
}
function uploadPoleImg($slc_id,$file)
{
	$pole_image = "";
	if(isset($file['pole_image']['name']) && $file['pole_image']['name']!="")
	{
		$name 			= basename($file['pole_image']['name']);
		$extention 		= pathinfo($name, PATHINFO_EXTENSION);
		$now_fileName 	= 'SLC-'.$slc_id.'-Pole-'.time().".".$extention;
		$uploadfile 	= ROOT_IMG_PATH . $now_fileName;
		if(@move_uploaded_file($file['pole_image']['tmp_name'], $uploadfile)) 
		{
			$pole_image = $now_fileName;
		}
	}
	return $pole_image;
}
function getSLCCount($cid)
{
	$db 	= 	getConnection();
	$sql 	=	"SELECT count(a.id) as tot FROM slc_installation_data AS a where a.client_id = ".$cid;
	$stmt	=	$db->query($sql);
	$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	return isset($data[0]->tot) && $data[0]->tot > 0 ?"Yes":"No";
}
function preSLCData($cid)
{
	global $cnf;
	$db 	= 	getConnection();
	$sql 	=	"SELECT a.* FROM slc_installation_data AS a where a.client_id = ".$cid." order by a.ID DESC LIMIT 1";
	$stmt	=	$db->query($sql);
	$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	return isset($data[0])?$data[0]:array();
}
function checkPoleID()
{
	global $cnf;
	$data		= 	Slim::getInstance()->request()->post();
	$client_id	=	isset($data['client_id']) && !empty($data['client_id']) ? $data['client_id']:'';
	$user_id	=	isset($data['user_id']) && !empty($data['user_id']) ? $data['user_id']:'';
	$pole_id	=	isset($data['pole_id']) && !empty($data['pole_id']) ? $data['pole_id']:'';
	$lvars		=	getLanguage($user_id,'None');
	$clientinfo = 	getClientInfo($client_id);
	if(isset($clientinfo) && !empty($clientinfo) && isset($user_id) && !empty($user_id))
	{
		$gump 		= new GUMP();
		$validdata 	= $gump->sanitize($data);
		$gump->validation_rules(array('pole_id'=> 'required'));
		$gump->filter_rules(array('pole_id'=> 'trim'));
		$validated_data = $gump->run($validdata);
		if($validated_data === false) 
		{
			$errors = $gump->get_readable_errors(true, '', '', $user_id);
			echo json_encode(array("status" => 'error',"msg" => $errors));
		}
		else
		{
			/*$check = checkPoleIDUniqueness($pole_id,$client_id);
			if(isset($check) && $check > 0)
			{
				echo json_encode(array("status" => 'error','msg'=> $lvars['POLE_ID_ALREADY_USED']));
			}
			else
			{*/
				echo json_encode(array("status" => 'success'));
			//}
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}	
	exit();
}
function checkPoleIDUniqueness($pole_id,$client_id)
{
	$db = getConnection();
	$sql = "SELECT count(id) as tot FROM slc_installation_data where pole_id = '".$pole_id."' and client_id=".$client_id;
	$stmt = $db->query($sql);
	$result = $stmt->fetchAll(PDO::FETCH_OBJ);
	$db = null;
	//return isset($result[0]->tot) ? $result[0]->tot : 0;
}
function syncDataToLGServer()
{
	//call LG Server
	set_time_limit(0);
	global $cnf;
	$starttime = date("Y-m-d H:i:s");
	$db 	= 	getConnection();
	$lvars	=	getLanguage('None','None');
	$sql 	=	"SELECT s.clientid FROM slc_sync AS s where s.status = 'Process' GROUP BY s.clientid ORDER BY s.id ASC Limit 1";
	$stmt	=	$db->query($sql);
	$clientdata	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
	if(count($clientdata) > 0)
	{
		foreach($clientdata as $cval)
		{
			$sql 	=	"SELECT 
								s.id as SSID, a.ID,a.client_id,a.user_id,a.mac_address,a.slc_id,a.lat,a.lng,a.address,a.measurement_units,a.pole_id,a.pole_image,a.date_of_installation,a.assets
						 FROM 
								slc_sync AS s
								LEFT JOIN slc_installation_data AS a ON a.id = s.slcid
						where
								s.status = 'Process'
								and
								s.clientid = ".$cval->clientid."
						ORDER BY 
								s.id ASC		
						Limit
								1
								";
								
			$stmt	=	$db->query($sql);
			$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ);
			//check data empty or not	
			if(count($data) > 0)
			{
				$clientinfo 			= 	getLGClientInfo($cval->clientid);

				$sql_token 	=	"SELECT * FROM slc_users WHERE client_id = ".$cval->clientid." ORDER BY id DESC Limit 1";
				$stmt_token	=	$db->query($sql_token);
				$tokendata	= 	$stmt_token->fetchAll(PDO::FETCH_OBJ);

				//echo $tokendata[0]->token;//exit;
				//echo "<pre>";print_r($tokendata);exit;
				//options array
				$options 				= 	array();
				$options['Count'] 		= 	count($data);
				if($clientinfo->vLGVersion == 'LG5'){
					$ClientID1 = $clientinfo->clientid;
				}else{
					$ClientID1 = encryption($clientinfo->clientid);
				}

				$options['ClientID'] 	= 	$ClientID1;
				//slc info
				foreach($data as $key=>$val)
				{
					$userInfo 				=	getUserInfo($val->user_id);
					$SLCInfo = array();
					$SLCInfo['SLCID'] 		= $val->slc_id;
					$SLCInfo['MACAddress'] 	= $val->mac_address;
					$SLCInfo['Latitude'] 	= substr($val->lat, 0, 14);
					$SLCInfo['Longitude'] 	= substr($val->lng, 0, 14);
					$SLCInfo['Address'] 	= $val->address;
					$SLCInfo['PoleID'] 		= $val->pole_id;
					$SLCInfo['PoleImage'] 	= isset($val->pole_image) && !empty($val->pole_image) ? IMG_URL.$val->pole_image : "";
					$SLCInfo['DateOfInstallation'] = $val->date_of_installation;
					//assets
					$AssetInfo = array();
					if(isset($val->assets))
					{
						$AssetInfo = getAssetsInfoArrayConvert($val->assets);
					}
					//generate array
					$assetsInfoData = array('UserName'=>$userInfo->username ,'SLCInfo'=>$SLCInfo,'AssetInfo'=>$AssetInfo);
					$options['ListSLCAsset'][$key] = $assetsInfoData;
				}
				//get client information
				$data_string = json_encode($options);
				//echo $clientinfo->api_server;exit;
				//echo "<pre>";print_r($data_string);exit;
				//post data to LG Server
				$headers = array('Content-Type:application/json');
				$ch = curl_init();
				curl_setopt($ch,CURLOPT_URL,$clientinfo->api_server.'post-slc-data-modified');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				//curl_setopt($ch,CURLOPT_POST, true);
				curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
				//curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
				if($clientinfo->vLGVersion == 'LG5'){
					curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json,Content-Length:'.strlen($data_string),'Authorization: bearer '.$tokendata[0]->token, 'IsSecured:1'));
				}else{
					curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json,Content-Length:'.strlen($data_string, 'IsSecured:1')));
				}
				//curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, true);
				curl_setopt($ch,CURLOPT_POSTFIELDS, $data_string);
				$response = curl_exec($ch);
				$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				if ($response === FALSE) 
				{
					//generate log
					$filename = ROOT_LOGSPATH.'SLC-SYNC-LG-FAIL-'.time().'.txt';
					$logfile = fopen($filename,"wb");
					fwrite($logfile,json_encode(array('response'=>$response,'data'=>$options, 'starttime'=>$starttime, "endtime"=>date("Y-m-d H:i:s"))));
					fclose($logfile);
				}
				curl_close($ch);
				$OutputInfo = json_decode($response);
				//generate log
				$filename = ROOT_LOGSPATH.'SLC-SYNC-LG-SUCCESS-'.time().'.txt';
				$logfile = fopen($filename,"wb");
				fwrite($logfile,json_encode(array('response'=>$response,'data'=>$options ,'httpcode'=>$httpcode, 'starttime'=>$starttime, "endtime"=>date("Y-m-d H:i:s"))));
				fclose($logfile);
				//return response
				if($httpcode == 200)
				{
					foreach($OutputInfo as $oval)
					{
						//echo "<pre>";print_r($OutputInfo);exit;
						$slcresponse	=	json_encode($oval);
						if($clientinfo->vLGVersion == 'LG5'){
							$SLCID			=	$oval->slcid;
							$OutputInfo1	=	$oval->outputInfo;
						}else{
							$SLCID			=	$oval->SLCID;
							$OutputInfo1	=	$oval->OutputInfo;
						}
						$sql 			=	"SELECT a.ID FROM slc_installation_data AS a WHERE a.client_id = ".$cval->clientid." and a.slc_id = '".$SLCID."'";
						$stmt			=	$db->query($sql);
						$slcdata		= 	$stmt->fetchAll(PDO::FETCH_OBJ);
						
						if(isset($OutputInfo1) && ($OutputInfo1 == "Inserted" || $OutputInfo1 == "Updated" || $OutputInfo1 == "Already Exists"))
						{
							//update slc status
							$sql = "UPDATE slc_installation_data SET sync = 'Success' WHERE ID = ".$slcdata[0]->ID;
							$stmts = $db->query($sql);
							//update sync status
							$sql = "UPDATE slc_sync SET status = 'Complete', completed = '".date('Y-m-d H:i:s')."', response = '".addslashes($slcresponse)."' WHERE clientid = ".$cval->clientid." and status = 'Process' and slcid=".$slcdata[0]->ID;
							$stmts = $db->query($sql);
							
							//update approved status
							$sql = "UPDATE slc_approve SET status = 'Complete', response_time = '".date('Y-m-d H:i:s')."', response = '".addslashes($slcresponse)."' WHERE clientid = ".$cval->clientid." and status = 'Process' and slcid=".$slcdata[0]->ID;
							$stmts = $db->query($sql);
						}
						else
						{
							//update slc status
							$sql = "UPDATE slc_installation_data SET sync = 'Fail', isApproved = 'No' WHERE ID = ".$slcdata[0]->ID;
							$stmts = $db->query($sql);
							
							//update sync status
							$sql = "UPDATE slc_sync SET status = 'Fail', completed = '".date('Y-m-d H:i:s')."', response = '".addslashes($slcresponse)."' WHERE clientid = ".$cval->clientid." and status = 'Process' and slcid=".$slcdata[0]->ID;
							$stmts = $db->query($sql);
							
							//update approved status
							$sql = "UPDATE slc_approve SET status = 'Fail', response_time = '".date('Y-m-d H:i:s')."', response = '".addslashes($slcresponse)."' WHERE clientid = ".$cval->clientid." and status = 'Process' and slcid=".$slcdata[0]->ID;
							$stmts = $db->query($sql);
						}
					}
					echo json_encode(array("status" => 'success','msg'=>'Success', 'starttime'=>$starttime, "endtime"=>date("Y-m-d H:i:s")));
				}
				else
				{
					echo json_encode(array("status" => 'error','msg'=>'Error' ,'httpcode'=>$httpcode, 'response'=>$response, "endtime"=>date("Y-m-d H:i:s")));	
				}
			}
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_NOT_FOUND']));
	}
	exit();
}
function getSLCFromLG($clientID)
{
	global 	$cnf;
	$lvars		=	getLanguage('None','None');
	$db 		= 	getConnection();
	$clientinfo = 	getLGClientInfo($clientID);
	if(isset($clientinfo) && !empty($clientinfo))
	{
		$clientid	= 	encryption($clientinfo->clientid);
		set_time_limit(0);
		//post data to LG Server
		$headers = array('Content-Type:application/json');
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$clientinfo->api_server.'Get-Existing-SLCs-For-A-Client');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		//curl_setopt($ch,CURLOPT_POST, true);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Type: application/json','clientid:'.$clientid, 'IsSecured:1'));
		$response = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if ($response === FALSE) 
		{
			//generate log
			$filename = ROOT_LOGSPATH.'SLC-SYNC-LG-FAIL-'.time().'.txt';
			$logfile = fopen($filename,"wb");
			fwrite($logfile,json_encode(array('response'=>$response)));
			fclose($logfile);
		}
		curl_close($ch);
		$OutputInfo = json_decode($response);
		//return response
		if($httpcode == 200)
		{
			echo json_encode(array("status" => 'error','data'=>$OutputInfo));
		}
		else
		{
			echo json_encode(array("status" => 'error','msg'=>$lvars['SLC_LIST_NOT_FOUND']));
		}
	}
	else
	{
		echo json_encode(array("status" => 'error','msg'=>$lvars['INVALID_CLIENT']));
	}
	exit();
}
function getpoleoptions(){
	$PoleOptions = array();

	$options['data'][0]['key'] 		= 'Camera';
	$options['data'][0]['value'] 	= 'Camera';

	$options['data'][1]['key'] 		= 'AirQualityMonitor';
	$options['data'][1]['value'] 	= 'AirQuality Monitor';

	$options['data'][2]['key'] 		= 'Microphone';
	$options['data'][2]['value'] 	= 'Microphone';

	$options['data'][3]['key'] 		= 'Radar';
	$options['data'][3]['value'] 	= 'Radar (Speed)';

	$options['data'][4]['key'] 		= 'LTRLicensePlateReader';
	$options['data'][4]['value'] 	= 'LTR License Plate Reader';

	$options['data'][5]['key'] 		= 'WeatherStation';
	$options['data'][5]['value'] 	= 'Weather Station';

	$options['data'][6]['key'] 		= 'PeopleCounter';
	$options['data'][6]['value'] 	= 'People Counter';

	$options['data'][7]['key'] 		= 'CarCounter';
	$options['data'][7]['value'] 	= 'Car Counter';

	$options['data'][8]['key'] 		= 'EVCharger';
	$options['data'][8]['value'] 	= 'EV Charger';

	$options['data'][9]['key'] 		= 'RoadTempMonitor';
	$options['data'][9]['value'] 	= 'Road Temp Monitor';

	$options['data'][10]['key'] 	= 'LEDDisplay';
	$options['data'][10]['value'] 	= 'LED Display';

	$options['data'][11]['key'] 	= 'SmallCellularAntenna';
	$options['data'][11]['value'] 	= 'Small Cellular Antenna';

	$options['data'][12]['key'] 	= 'FirstResponderAntenna';
	$options['data'][12]['value'] 	= 'First Responder Antenna';

	$options['data'][13]['key'] 	= 'PowerMeter';
	$options['data'][13]['value'] 	= 'Power Meter';

	$options['data'][14]['key'] 	= 'Other';
	$options['data'][14]['value'] 	= 'Other';

	echo json_encode($options);
	exit();
}
function changeLanguage($UserID,$Lan,$ClientID)
{
	global $cnf;
	$db		= getConnection();

	$sql1 = "SELECT typeid FROM slc_clients as a WHERE a.id = ".$ClientID;
	$stmt   = $db->query($sql1);
	$data	= 	$stmt->fetchAll(PDO::FETCH_OBJ); 

	$sql 	= "UPDATE slc_users SET language = '".$Lan."' WHERE id = ".$UserID;
	$stmts = $db->query($sql);
	//db null
	$db = null;
	$lvars 	= getLanguage($UserID,'None');
	if (isset($data) && count($data) > 0)
  	{
  		$result =   array();
		if($data[0]->typeid == 4){
			$result['ScanLBL'] =  $lvars['EU_ID'];
			$result['ScanPH'] =  $lvars['EU_ID'];
		}else if($data[0]->typeid == 5){
			$result['ScanLBL'] =  'NID';
			$result['ScanPH'] =  'NID';
		}else if($data[0]->typeid == 7){
			$result['ScanLBL'] =  'IMEI';
			$result['ScanPH'] =  'IMEI';
		}else if($data[0]->typeid == 8){
			$result['ScanLBL'] =  'UID';
			$result['ScanPH'] =  'UID';
		}else{
			$result['ScanLBL'] =  $lvars['MAC_ID'];
			$result['ScanPH'] =  $lvars['MAC_ID'];
		}
  	}

	echo json_encode(array("status" => 'success','msg'=>'success','language'=>$Lan,'ScanLBL'=>$result['ScanLBL'],'ScanPH'=>$result['ScanPH']));
}
function getLanguage($UserID="None",$DefaultLan="None")
{
	include_once('ini.php');
	if(isset($UserID) && $UserID != 'None' && $UserID > 0)
	{
		$info = getUserInfo($UserID);
		$lan = isset($info->language)?$info->language:"en";
		if($lan == 'es')
		{
			$language = INI::read('language.ini');
			$lvars = $language['Spanish'];
		}
		else if($lan == 'pt')
		{
			$language = INI::read('language.ini');
			$lvars = $language['Protugues'];
		}
		else
		{
			$language = INI::read('language.ini');
			$lvars = $language['english'];
		}
	}
	else
	{
		if(isset($DefaultLan) && !empty($DefaultLan) && $DefaultLan != 'None')
		{
			if($DefaultLan == 'es')
			{
				$language = INI::read('language.ini');
				$lvars = $language['Spanish'];
			}
			else if($DefaultLan == 'pt')
			{
				$language = INI::read('language.ini');
				$lvars = $language['Protugues'];
			}
			else
			{
				$language = INI::read('language.ini');
				$lvars = $language['english'];
			}
		}
		else
		{
			$language = INI::read('language.ini');
			$lvars = $language['english'];
		}
	}
	return $lvars;
}
function Haversine($lat_from, $lon_from, $lat_to, $lon_to) {
    $radius = 6371000;
    $delta_lat = deg2rad($lat_to-$lat_from);
    $delta_lon = deg2rad($lon_to-$lon_from);

    $a = sin($delta_lat/2) * sin($delta_lat/2) +
        cos(deg2rad($lat_from)) * cos(deg2rad($lat_to)) *
        sin($delta_lon/2) * sin($delta_lon/2);
    $c = 2*atan2(sqrt($a), sqrt(1-$a));

    // Convert the distance from meters to miles
    return ceil(($radius*$c)*0.000621371);
}
function topbottomlatlng() {
	global $cnf;
	$db 			= 	getConnection();
    //echo $sql 	= "SELECT * FROM slc_installation_data WHERE lat BETWEEN '39.946780595531465' AND '3.6115853419711925' AND lng between '84.010743745082777' AND '61.153330987560196' LIMIT 100";
	//echo $sql 	= "SELECT * FROM slc_installation_data WHERE lat > '39.946780595531465' AND lat < '3.6115853419711925' AND lng > '84.010743745082777' AND lng < '61.153330987560196' LIMIT 100";
	$sql = "SELECT id, address, lat, lng FROM slc_installation_data WHERE
			('39.946780595531465' < '3.6115853419711925' AND lat BETWEEN '39.946780595531465' AND '3.6115853419711925') OR 
			('3.6115853419711925' < '39.946780595531465' AND lat BETWEEN '3.6115853419711925' AND '39.946780595531465')
			AND 
			('84.010743745082777' < '61.153330987560196' AND lng BETWEEN '84.010743745082777' AND '61.153330987560196') OR 
			('61.153330987560196' < '84.010743745082777' AND lng BETWEEN '61.153330987560196' AND '84.010743745082777')
			AND client_id = 83 LIMIT 100";
	$stmt	= $db->query($sql);
	$data	= $stmt->fetchAll(PDO::FETCH_OBJ);
	echo "<pre>";print_r($data);exit;
}